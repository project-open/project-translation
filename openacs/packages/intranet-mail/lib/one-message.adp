<div style="background-color: #eee; padding: .5em;">
<table>
<tr><td>
#intranet-mail.Sent_Date#:</td><td>@sent_date_pretty;noquote@</tr><td>
#intranet-mail.Sender#:</td><td>@sender;noquote@</tr><td>
#intranet-mail.Recipient#:</td><td>@recipient;noquote@</tr><td>
#intranet-mail.CC#:</td><td>@cc_string;noquote@</tr><td>
#intranet-mail.BCC#:</td><td>@bcc_string;noquote@</tr><td>
#intranet-mail.Subject#:</td><td>@subject;noquote@</tr><td>
#intranet-mail.Attachments#:</td><td>@download_files;noquote@</tr><td>
#intranet-mail.MessageID#:</td><td>@message_id;noquote@</tr>
</table>
</div>
<p>
@body;noquote@
