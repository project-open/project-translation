ad_page_contract {
    @author ND
} {
}

set form_id "invoice_download"

set month_names [dt_month_names]
set month_options [list [list "All" ""]]
for {set i 0} {$i < 12} {incr i} {
    lappend month_options [list [lindex $month_names $i] [expr $i+1]]
}

set current_year [ns_fmttime [ns_time] %Y] 
set current_month [ns_fmttime [ns_time] %M] 

set year_options [list]

db_foreach year {
    select distinct to_char(effective_date,'YYYY') as invoice_year from im_costs where effective_date is not null order by invoice_year desc
} {
    lappend year_options [list $invoice_year $invoice_year]
}

set type_options [list [list "All" ""]]
foreach cost_type_id [list [im_cost_type_invoice] [im_cost_type_quote] [im_cost_type_bill] [im_cost_type_po]] {
    lappend type_options [list [im_category_from_id $cost_type_id] $cost_type_id]
}
ad_form -name $form_id -action "/intranet-invoices/invoice-download" -form {
    {year:text(select)
        {label "Year"}
        {options $year_options}
    }
    {month:text(select),optional
        {label "Month"}
        {options $month_options}
    }
    {cost_type_id:text(select),optional 
        {label \#intranet-invoices.Document_Type\#} 
        {value [im_cost_type_invoice]} 
        {options $type_options}
    }
        
} -on_submit {

    if { $month eq {} } {
        set sql "
            select invoice_id, invoice_nr, extract(month from effective_date) as invoice_month
            from im_invoices inv inner join im_costs c on (inv.invoice_id=c.cost_id)
            where extract ( year from effective_date ) = :year
        "
        set prev_month ""
        if {$cost_type_id ne ""} {
            append sql " and c.cost_type_id = :cost_type_id"
        }

        db_foreach invoice $sql {
    
            set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]
    
            if { "" == $invoice_item_id } {
                # set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id]
                continue
            } else {
                set invoice_revision_id [content::item::get_best_revision -item_id $invoice_item_id]
            }
    
            set filename [content::revision::get_cr_file_path -revision_id $invoice_revision_id]
    
            if { $prev_month ne $invoice_month } {
                set prev_month $invoice_month
                set month_dir "/tmp/${year}_invoices/${invoice_month}"
                file mkdir $month_dir
            }
    
            if { [file exists $filename] } {
                file copy -force $filename ${month_dir}/${invoice_nr}.pdf
            }
        }
    
        set zipfile /tmp/${year}_invoices.zip
        cd /tmp
        exec zip -r $zipfile ${year}_invoices
        file delete -force ${year}_invoices
    
    } else {
    
        set sql "
            select invoice_id, invoice_nr, effective_date
            from im_invoices inv inner join im_costs c on (inv.invoice_id=c.cost_id)
            where extract ( year from effective_date ) = :year
            and extract(month from effective_date) = :month
        "
    
        if {$cost_type_id ne ""} {
            append sql " and c.cost_type_id = :cost_type_id"
        }
        set month_dir "/tmp/${year}_${month}_invoices"
        file mkdir $month_dir
        db_foreach invoice $sql {
    
            set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]
    
            if { "" == $invoice_item_id } {
                # set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id]
                continue
            } else {
                set invoice_revision_id [content::item::get_best_revision -item_id $invoice_item_id]
            }
    
            set filename [content::revision::get_cr_file_path -revision_id $invoice_revision_id]
    
            if { [file exists $filename] } {
                file copy -force $filename ${month_dir}/${invoice_nr}.pdf
            }
        }
    
        set zipfile /tmp/${year}_${month}_invoices.zip
        cd /tmp
        exec zip -r $zipfile ${year}_${month}_invoices
        file delete -force ${year}_${month}_invoices
    }
    
} -after_submit {
    ns_returnfile 200 application/zip $zipfile    
}


