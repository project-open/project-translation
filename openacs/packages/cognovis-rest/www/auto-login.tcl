ad_page_contract {

	Returns the Token and API Key for the current user	

	@author malte.sussdorff@cognovis.de based upon work from 
    @author frank.bergmann@project-open.com
} {
}

set user_id [auth::require_login]

set auto_login [im_generate_auto_login -user_id $user_id]

set api_key [base64::encode "${user_id}:$auto_login"]

set username ""
db_0or1row user_info "
	select	username
	from	cc_users
	where	user_id = :user_id
"




