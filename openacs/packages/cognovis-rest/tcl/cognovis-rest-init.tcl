ad_library {

    Initialization for cognovis-rest module
    
    @author malte.sussdorff@cognovis.de

}

namespace eval cog_rest {}
namespace eval cog_rest::post {}
namespace eval cog_rest::get {}
namespace eval cog_rest::put {}
namespace eval cog_rest::delete {}
namespace eval cog_rest::json_object {}

# Register handler procedures for the various HTTP methods
ns_register_proc GET /cognovis-rest/* cog_rest::get::call
ns_register_proc POST /cognovis-rest/* cog_rest::post::call
ns_register_proc PUT /cognovis-rest/* cog_rest::put::call
ns_register_proc DELETE /cognovis-rest/* cog_rest::delete::call

ns_register_proc POST /cognovis-rest/upload cog_rest::post::upload_file
ns_register_proc GET /cognovis-rest/status cog_rest::get::upload_status
ns_register_filter preauth POST /cognovis-rest/upload/* cog_rest::post::upload_update

# CORS support for options
ns_register_proc OPTIONS /cognovis-rest/* ::my_options_handler

proc ::my_options_handler args {
    ns_set put [ns_conn outputheaders] "Access-Control-Allow-Origin" "*"
    ns_set put [ns_conn outputheaders] "Access-Control-Allow-Methods" "GET, POST, PUT, DELETE, OPTIONS"
    ns_set put [ns_conn outputheaders] "Access-Control-Allow-Headers" "DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization"
    ns_set put [ns_conn outputheaders] "Access-Control-Max-Age" 1728000
    ns_set put [ns_conn outputheaders] "Content-Type" "text/plain; charset=utf-8"
    ns_set put [ns_conn outputheaders] "Content-Length" 0
    ns_return 204 text/plain {}
}