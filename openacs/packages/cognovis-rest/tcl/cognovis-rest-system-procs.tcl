ad_library {
	@author malte.sussdorff@cognovis.de
}
namespace eval cog_rest::json_object {
    ad_proc user_token {} {
        @return user_id number UserID of the user in case of success
        @return token string Token of the user_id requested
        @return bearer_token Bearer token used now instead of user_id+token.
    } -
}

ad_proc -public cog_rest::post::upgrade_system {
    -rest_user_id:required
    -server_url:required
    -server_path:required
    { -system_owner ""}
    { -develop_p "0"}
} {
    Upgrades the system e.g. from a production database

    @param server_url string URL of the server
    @param server_path string PATH of the filesystem for the server
    @param system_owner string E-Mail of the system owner
    @param develop_p boolean Is this a develop system
} {
    if {$system_owner eq ""} {
        set system_owner [ad_system_owner]
    }
    if {$develop_p} {
        im_sysconfig_change_server -server_path $server_path-server_url $server_url -system_owner $system_owner -develop
    } else {
        im_sysconfig_change_server -server_path $server_path-server_url $server_url -system_owner $system_owner
    }

    return [cog_rest::post::update_system -rest_user_id $rest_user_id]
}

ad_proc -public cog_rest::post::update_system {
    { -package_key "" }
    -rest_user_id:required
} {
    Update the packages of the system

    @param package_key string Key of the package to update. If not provided, update all.

    @return packages Array of package keys to update
    @return_packages package_key string Key of the package which can be updated
    @return_packages package_name string Name of the package (more extensive description)
    @return_packages new_version string Version number of the new package
    @return_packages installed_version string Version number of the currently installed package
    @return_packages err_msg string Message of the error if we were not successful in upgrading the package
} {
    set errors [list]
    set packages [list]

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -http_status 401 -message "User must be a site wide admin to know which packages to update"
    }

    # Are there packages to upgrade?
    set packages_to_upgrade [cog::packages_to_upgrade -package_key $package_key]
    
    # Dependency check
    apm_get_package_repository -array repository
    apm_get_installed_versions -array installed

    cog_log notice "cog_rest::update_system - run apm_dependency_check_new with <$packages_to_upgrade>"
    array set result [apm_dependency_check_new \
                        -repository_array repository \
                        -package_keys $packages_to_upgrade]
    cog_log notice "cog_rest::update_system -  apm_dependency_check_new with <$packages_to_upgrade>: [array get result]"
    
    if {$result(status) eq "ok"} {
        #
        # Do the upgrade
        #
        foreach package_key $result(install) {
            #
            # As we may have new packages included by the dependency check,
            # determine if we are upgrading or installing.
            #
            set spec_file       [apm_package_info_file_path $package_key]
            array set version   [apm_read_package_info_file $spec_file]
            set new_version     $version(name)
            set package_name $version(package-name)

            # Check on the installed version vs. new install (empty installed)
            if { [apm_package_upgrade_p $package_key $new_version] == 1} {
                set installed_version [apm_highest_version_name $package_key]
            } else {
                set installed_version ""
            }

            #
            # Select SQL scripts
            #
            set data_model_files [apm_data_model_scripts_find \
                                    -upgrade_from_version_name $installed_version \
                                    -upgrade_to_version_name $new_version \
                                    $package_key]
        
            # Upgrade the package
            if {[catch {
                set version_id [apm_package_install \
                        -enable=1 \
                        -load_data_model \
                        -data_model_files $data_model_files \
                        $spec_file]

                # Upgrade successful
                set err_msg ""
                lappend packages [cog_rest::json_object]
            } errorMsg]} {
                # Upgrade failed
                set err_msg "$errorMsg : $::errorInfo"
                set new_vesion ""
                lappend errors [cog_rest::json_object]

                cog_log Error "cog_rest::update_system : $package_key $errorMsg\n [ns_quotehtml $::errorInfo]"
            }
        }
    } else {

        # Dependency check error
        cog_rest::error -http_status 500 -message "* Dependency check failed\n"
    }
    return [cog_rest::return_array]
}

namespace eval cog_rest::get {

        ad_proc -public user_token {
        -user_id:required
        -rest_user_id:required
    } {
        Return the user_id and token for a user

        Only for site wide admins though.

        @author Malte Sussdorff (malte.sussdorff@cognovis.de)

        @param user_id number UserID to switch into

        @return user json_object user_token Object with user info and token
    } {

        if {![acs_user::site_wide_admin_p -user_id $rest_user_id]} {
            set user_id $rest_user_id
        } 

        cog_log Warning "[im_name_from_id $rest_user_id] ($rest_user_id) retrieved token for [im_name_from_id $user_id] ($user_id)"

        set token [im_generate_auto_login -user_id $user_id]
        set bearer_token [base64::encode "${user_id}:$token"]
        
        # set result "{\"success\": true, \n\"user_id\": $user_id, \n\"token\": \"$token\"\n}"
        set user [cog_rest::json_object]
        return [cog_rest::json_response]
    }
        
    ad_proc -public switch_user {
        -switch_user_id
        { -format "json" }
        { -rest_user_id 0 }
        { -rest_otype "" }
        { -rest_oid "" }
        { -debug 0 }
        { -query_hash_pairs ""}
    } {
        Return the user_id and token for a user to switch into

        Only for site wide admins though.

        @author Malte Sussdorff (malte.sussdorff@cognovis.de)

        @param switch_user_id UserID to switch into
        @param_json switch_user_id number User

        @return switched_user single json object with user and token info.
        
        @return_switched_user user_id number UserID of the user in case of success
        @example_switched_user user_id 899
        @return_switched_user token string Token of the user_id requested
        @example_switched_user token B6589FC6AB0DC82CF12091D1C2D40AB984E8410C
        @return_switched_user bearer_token Bearer token used now instead of user_id+token.
        @example_switched_user bearer_token MTEyOTI3NjpERTZCMEJCOEI0QTkzMjdFNjc1MEIwOTcwNEFENjI2NjU4NTUwNDc2
    } {

        set switching_users [list]    
        if {![acs_user::site_wide_admin_p -user_id $rest_user_id]} {
            set user_id $rest_user_id
        } else {
            set user_id $switch_user_id
        }

        set token [im_generate_auto_login -user_id $user_id]
        set bearer_token [base64::encode "${user_id}:$token"]
        
        # set result "{\"success\": true, \n\"user_id\": $user_id, \n\"token\": \"$token\"\n}"
        set result [cog_rest::json_object]
        return $result
    }
    
    ad_proc -public enums {
        { -enum_type "" }
        { -language "typescript"}
        -rest_user_id
    } {

        Return the enums for inclusion

        @author malte.sussdorff@cognovis.deal
        @creation_date 2020-09-07

        @param enum_type string Type of enums we want to return. Typically categories (only supported one)
        @param language string (Programming) Language for which to generate the enums. currently defaults to typescript (which is the only one suppported)

        @return string of enums
    } {
        if {![im_user_is_admin_p $rest_user_id]} {
            return [cog_rest::error -http_status 401 -message "User is not authorized to view this page"]
        }

        if {$enum_type eq ""} {
            set enum_code [cog_rest::build::category_enums -language $language]
            append enum_code "\n\n[cog_rest::build::country_enums -language $language]"
        } else {
            switch $enum_type {
                categories {
                    set enum_code [cog_rest::build::category_enums -language $language]
                }
                default {
                    set enum_code "$enum_type is not supported yet"
                }
            }
        }

        
        ns_return 200 "text/plain" $enum_code

        # Return empty string
        return ""
    }

    ad_proc -public auth_token {
        -username:required
        -password:required
    } {
        Return the token information for the user upon login

        @param username string Username of the user. 
        @param password string Password for the user

        @return JSON with token, user_id and bearer_token
    } {

        if {$username ne "" && $password ne ""} {
            # Try to login the user
            array set auth_info [auth::authenticate -email $username -password $password -no_cookie]
            if {$auth_info(auth_status) ne "ok"} {
                cog_rest::error -http_status 401 -message "$auth_info(auth_message)"
            } else {
                set token [im_generate_auto_login -user_id $auth_info(user_id)]
                set bearer_token [base64::encode "${auth_info(user_id)}:$token"]
                return "{\"success\": true,\n\"token\": \"$token\",\n\"message\": \"user authenticated\", \n\"bearer_token\": \"$bearer_token\", \n\"user_id\": $auth_info(user_id)}"
            }
        } else {
            cog_rest::error -http_status 401 -message "user not logged in"
        }
    }


    ad_proc -public server_up {

    } {
        @return server json_object
        @return_server success boolean Returns true if we could successfully access the database
    } {
        set success 0
        
        catch {set success [db_string database_check "select 1 from dual" -default 0]}

        if {!$success} {
            cog_rest::error -http_status 503 -message "Database not reachable"
        } else {
            return "{\"success\": true}"

        }
    }

    ad_proc -public update_system {
        -rest_user_id:required
    } {
        Returns a list of package_keys which can be updated. Only valid for site wide admins

        @return packages Array of package keys to update
        @return_packages package_key string Key of the package which can be updated
        @return_packages package_name string Name of the package (more extensive description)
        @return_packages new_version string Version number of the new package
        @return_packages installed_version string Version number of the currently installed package
    } {
        if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
            cog_rest::error -http_status 401 -message "User must be a site wide admin to know which packages to update"
        }


        set packages [list]

        #---------------------------------------------------------------
        # Loop through all upgradeable packages and return the ones we can upgrade
        #---------------------------------------------------------------
        foreach package_key [cog::packages_to_upgrade] {
            # Get Information from the package file            
            set spec_file       [apm_package_info_file_path $package_key]
            array set version   [apm_read_package_info_file $spec_file]
            set new_version  $version(name)
            set installed_version [apm_highest_version_name $package_key]
            set package_name $version(package-name)
            lappend packages [cog_rest::json_object]
        }
        return [cog_rest::return_array]
    }


    ad_proc -public package_parameter {
        -rest_user_id:required
        { -package_id ""}
        { -package_keys ""}
    } {
        Handler for GET rest calls to get package parameters 
        It is possible to query with package_id or package_key
        If neither of these will be provided, endpoint will return all parameters from system


        @param package_id integer id of package of needed package parameters
        @param package_keys json_array array of package keys of needed package parameters
        @param_package_keys key string actual package key

        @param rest_user_id integer used to check that only site wide admins can access this

        @return parameters json_array parameter Array of package parameters
    } {
        set parameters [list]

        set package_parameters_sql "select p.package_id, pp.default_value, pp.parameter_id, pp.parameter_name, pp.package_key, pv.attr_value as value from apm_packages p, apm_parameters pp, apm_parameter_values pv where pp.parameter_id = pv.parameter_id and p.package_key = pp.package_key"
        if {$package_id ne ""} {
            append package_parameters_sql " and p.package_id =:package_id"
        } 

        
        if {$package_keys ne ""} {
            append package_parameters_sql " and pp.package_key in ([template::util::tcl_to_sql_list $package_keys])"
        } else {
            # Only sysadmins can view all parameters
            if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
                return [cog_rest::error -http_status 403 -message "User is not allowed to see parameters"]
            }
        }
        db_foreach package_data $package_parameters_sql {
            lappend parameters [cog_rest::json_object]
        }

        return [cog_rest::json_response]
    }

    ad_proc -public plugin {
        {-plugin_ids ""}
        {-project_id ""}
        {-page_url ""}
        {-return_url ""}
        -rest_user_id
    } {
        Provide HTML of a component plugin Call this with /cognovis-rest/plugin?plugin_id=23882&company_id=8711 So you can pass on all arguments as URL values 

        @param plugin_ids string String with comma separated plugin_ids
        @param return_url string URL to retun, used in plugins
        @param project_id object im_project::read Project_id which to look for the plugins
        @param page_url string URL to use for plugins instead of plugin_ids

        @return data data with plugins
        @return_data plugin_id integer ID of the plugin
        @return_data plugin_title string Title shown for the plugin
        @return_data plugin_name string Name of the plugin. Same as Title if the title does not contain a title_tcl
        @return_data plugin_html string HTML of the plugin with URL replacements being done
        @return_data sort_order integer Number of the plugin on the location (Y-Position)
        @return_data location string Location, typically one of left/right, but might also be top/buttom
    } {
        set user_id $rest_user_id

        # Check we have valid plugin_ids    
        if {$plugin_ids eq ""} {
            set plugin_ids [db_list plugins "select plugin_id from im_component_plugins where page_url = :page_url and enabled_p = 't' order by location, sort_order asc"]
            if {$plugin_ids eq ""} {
                cog_rest::error -http_status 403 -message "No Plugins found for $page_url"
            }
        } else {
            set plugin_ids [split $plugin_ids ","]
            set plugin_ids [db_list plugins "select plugin_id from im_component_plugins where plugin_id in ([template::util::tcl_to_sql_list $plugin_ids]) order by location, sort_order asc"]
            if {$plugin_ids eq ""} {
                cog_rest::error -http_status 403 -message "Problem calling for plugins. No valid plugins found."
            }
        }

        # Loop through each plugin
        set data [list]
        
        foreach plugin_id $plugin_ids {
            if {![db_0or1row compoent "select * from im_component_plugins where plugin_id =:plugin_id"]} {
                cog_rest::error -http_status 403 -message "Problem finding the pluging $plugin_id"
            }

            if {$title_tcl eq ""} {
                set plugin_title $plugin_name
            } else {
                if {[catch $title_tcl plugin_title]} {
                    cog_rest::error -http_status 403 -message "Problem Getting the title $title_tcl :: $title"
                }
            }

            if {[catch $component_tcl plugin_html]} {
                # cog_rest::error -http_status 403 -message "Problem calling $component_tcl ::: $plugin_html"
                cog_log Error "Problem calling $component_tcl ::: $plugin_html"
            }

            # Regsub common missing urls
            regsub -all {HREF=/} $plugin_html "target=_blank HREF=[ad_url]/" plugin_html
            regsub -all {HREF=\"/} $plugin_html "target=_blank HREF=\"[ad_url]/" plugin_html
            regsub -all {HREF='/} $plugin_html "target=_blank HREF='[ad_url]/" plugin_html
            regsub -all {action=/} $plugin_html "target=_blank action=[ad_url]/" plugin_html
            regsub -all {action=\"/} $plugin_html "target=_blank action=\"[ad_url]/" plugin_html
            regsub -all {action='/} $plugin_html "target=_blank action='[ad_url]/" plugin_html
            regsub -all {href=/} $plugin_html "target=_blank href=[ad_url]/" plugin_html
            regsub -all {href=\"/} $plugin_html "target=_blank href=\"[ad_url]/" plugin_html
            regsub -all {href='/} $plugin_html "target=_blank href='[ad_url]/" plugin_html
            regsub -all {src=\"/} $plugin_html "src=\"[ad_url]/" plugin_html
            regsub -all {src=/} $plugin_html "src=[ad_url]/" plugin_html
            regsub -all {src='/} $plugin_html "src='[ad_url]/" plugin_html
            
            regsub -all {\n} $plugin_html {} plugin_html
            regsub -all {\t} $plugin_html {} plugin_html

            lappend data [cog_rest::json_object]

        }
        return [cog_rest::return_array]
    }
}