
ad_library {
    Procedures for supporting with materials
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::material {
    ad_proc -public get_material_id {
        -task_type_id:required
        -task_uom_id:required
        { -file_type_id ""}
        {-source_language_id ""}
        {-target_language_id ""}        
    } {
        Returns the material_id 
        Will use im_trans_material in case we have source + target_language_id
        @param task_type_id
        @param task_uom_id
        @param file_type_id 
        @param source_language_id
        @param target_language_id Language we want to translate into
    } {
        if {$source_language_id ne "" && $target_language_id ne ""} {
            set material_id [im_trans_material -task_type_id $task_type_id \
                -task_uom_id $task_uom_id \
                -file_type_id $file_type_id \
                -source_language_id $source_language_id \
                -target_language_id $target_language_id
            ]
        } else {
            if {$task_uom_id eq ""} {
                set material_id [db_string material "select material_id from im_materials
                    where task_type_id = :task_type_id
                    limit 1"]                
            } else {
                set material_id [db_string material "select material_id from im_materials
                    where task_type_id = :task_type_id
                    and material_uom_id = :task_uom_id
                    limit 1"]
            }
        }
        return $material_id
    }
}