ad_library {
    cognovis specific procedures for files
    
    @author malte.sussdorff@cognovis.de
}


namespace eval cog::file {
    ad_proc -public import_fs_file {
        -context_id:required
        -file_path:required
        {-filename ""}
        {-folder ""}
        {-user_id ""}
        {-description ""}
    } {
        Import a file to the content repository under the parent

        @param context_id integer Context we want to put the file under. Might be a content folder or an object
        @param folder string Folder which we want to put the file in.
        @param file_path string full path to the file we want to import
        @param filename string name of the file, will be deducted from file_path if not present
        @param description string Description of the file used in uploading
        @param user_id integer user who is doing the import. defaults to current user

        @return revision_id integer revision of the file
    } {
        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }
        set folder_id [cog::file::get_folder_id -folder $folder -context_id $context_id -create]

        if {$filename eq ""} {
            set filename [file tail $file_path]
        }

        set file_revision_id [cog::file::add -name $filename -parent_id $folder_id -creation_user $user_id -tmp_filename $file_path -description $description]
        content::item::set_live_revision -revision_id $file_revision_id

        return $file_revision_id
    }

    ad_proc -public add {
        -name:required
        -parent_id:required
        { -creation_user ""}
        -tmp_filename:required
        { -description "" }
    } {
        Adds a file to the content repository

        @param name Name of the file
        @param parent_id Which folder (or parent) is the file created in
        @param creation_user Who is creating the file (defaults to auth::get_user_id)
        @param tmp_filename where do we find the file
        @param description optional description for the file
    } {

        if {$creation_user eq ""} {
            set creation_user [auth::get_user_id]
        }

        set package_id [cog::package_id]
        set mime_type [cr_check_mime_type  -filename $name -mime_type "" -file $tmp_filename]
        if {[content::type::content_type_p -mime_type $mime_type -content_type "image"]} {
            set content_type image
        } else {
            set content_type file_storage_object
        }

        set item_id [db_string item "select item_id from cr_items where name = :name and parent_id = :parent_id" -default ""] 
        if {$item_id eq ""} {
            set item_id [db_nextval acs_object_id_seq]
            set item_id [content::item::new  -item_id $item_id  -parent_id $parent_id  -creation_user "$creation_user" -package_id $package_id -name $name  -storage_type "file"  -content_type "file_storage_object"  -mime_type "text/plain"]
        }
             
	    if {$creation_user ne ""} {
		    permission::grant -party_id $creation_user -object_id $item_id -privilege admin
	    }
        
        set revision_id [cog::file::add_version  -name $name  -parent_id $parent_id -tmp_filename $tmp_filename  -package_id $package_id  -item_id $item_id  -creation_user $creation_user -description $description  -mime_type $mime_type]
        return $revision_id
    }

    ad_proc -public add_version {
        -name:required
        -parent_id:required
        { -package_id "" }
        -item_id:required
        { -creation_user ""}
        -tmp_filename:required
        { -description "" }
        { -mime_type ""}
        
    } {
        Add a revision of a file
    } { 
        set mime_type [cr_check_mime_type  -filename $name -mime_type $mime_type -file $tmp_filename]
        set tmp_size [file size $tmp_filename]
        set parent_id [fs::get_parent -item_id $item_id]
        set creation_ip [ad_conn peeraddr]

        set revision_id [cr_import_content  -item_id $item_id  -storage_type file  -creation_user $creation_user -creation_ip $creation_ip -other_type "file_storage_object"  -image_type "file_storage_object"  -description $description  -package_id $package_id  $parent_id  $tmp_filename  $tmp_size  $mime_type  $name]
	
        db_exec_plsql update_last_modified {
            begin
                perform acs_object__update_last_modified
                (:parent_id,:creation_user,:creation_ip);
                perform
                acs_object__update_last_modified(:item_id,:creation_user,:creation_ip);
                return null;
            end;
        }
        content::item::set_live_revision -revision_id $revision_id
        return $revision_id
    }

    ad_proc -public get_folder_id {
        { -folder "" }
        -context_id:required
        -create:boolean
    } {
        Get the folder_id given a context

    } {
        set folder_id ""

        set object_type [ acs_object_type $context_id ]
        switch $object_type {
            im_company {
                set parent_id [ intranet_fs::get_company_folder_id -company_id $context_id ]
                set folder_id $parent_id
            }
            im_project {
                set parent_id [ intranet_fs::create_project_folder -project_id $context_id ]
                set folder_id $parent_id
            }
            default {
                set parent_id $context_id
                # check if the context is a folder
                if {[fs_folder_p $context_id]} {
                    set folder_id $context_id
                }
            }
        }

        if {$folder ne ""} {
            # Check if we have a root folder for this context.
            set folder_name [string tolower [util_text_to_url -text $folder]]
            set folder_id [fs::get_folder -name $folder_name -parent_id $parent_id]
            set package_id [db_string get_package_id " select package_id from apm_packages where package_key = 'file-storage' limit 1" ]

            if {$folder_id eq ""} {
                set folder_id [content::folder::new \
                    -name $folder_name \
                    -label $folder \
                    -parent_id $parent_id \
                    -context_id $context_id \
                    -creation_user [auth::get_user_id] \
                    -creation_ip [ns_conn peeraddr] \
                    -package_id $package_id
                ]
            }
        }

        if {$folder_id eq ""} {
            # Create a folder for the context
            set pretty_name [im_name_from_id $parent_id]
            set folder_name  [string tolower [util_text_to_url -text $pretty_name]]
            set folder_id [fs::get_folder -name $folder_name -parent_id $parent_id]

            if {$folder_id eq ""} {
                set folder_id $parent_id
            }
        }

        return $folder_id
    }
}

#---------------------------------------------------------------
# Filestorage
#---------------------------------------------------------------


ad_proc -public im_filestorage_mkdir {
	-dir
} {
	Creates the dir and all parent directories if necessary.
	
	Checks if the folder already exists!
	
	@return 1 if successful, errormessage otherwise
} {
	if { [catch { 
		if {![file exists $dir]} {
			cog_log Debug "exec /bin/mkdir -p $dir"
			exec /bin/mkdir -p $dir
			cog_log Debug "exec /bin/chmod ug+w $dir"
			exec /bin/chmod ug+w $dir
		}
	} err_msg]} { 
		return $err_msg
	} else {
		return 1
	}
}

ad_proc -public im_filestorage_rsync {
	-source_path
	-target_path
	-delete:boolean
} {
	Using Rsync -rz to sync the contents of source_path into target_path
	
	if delete is provided, then delete any files not in source_path
	
} {
	
	if {$delete_p} {
	    catch {exec rsync -rltzuv --delete ${source_path}/ $target_path}
	} else {
	    catch {exec rsync -rltzuv ${source_path}/ $target_path}
	}	
}


ad_proc im_filestorage_project_rest_component { 
    -project_id
    {-user_id ""} 
    {-return_url""}
} {
    Filestorage for projects using REST calls
} {
    set project_path [im_filestorage_project_path $project_id]
    if {$user_id eq ""} {
	set user_id [ad_conn user_id]
    }
    set folder_type "project"
    set project_name [im_name_from_id $project_id]
    set object_name [lang::message::lookup "" intranet-filestorage.Folder_type_Project "Project"]
    return [im_filestorage_base_component $user_id $project_id $object_name $project_path $folder_type]
}

ad_proc cog_filestorage_file_permissions {
    -user_id
    -object_id
    -file_path
} {
    Returns the permissions for a file which belongs to the object_id

    @param user_id user for which to check
    @param object_id object to use for the permission Checks
    @param file_path relative file path
    @return  Returns a list [v r w a]. 
} {
    
    # Get the list of all relevant roles and profiles for permissions
    set roles [im_filestorage_roles $user_id $object_id]
    set profiles [im_filestorage_profiles $user_id $object_id]

    # Get the group membership of the current (viewing) user
    set user_memberships [im_filestorage_user_memberships $user_id $object_id]

    # Get the list of all (known) permission of all folders of the FS of the current object
    set perm_hash_array [im_filestorage_get_perm_hash $user_id $object_id $user_memberships]
    array set perm_hash $perm_hash_array
    set user_perms [im_filestorage_folder_permissions $user_id $object_id $file_path $user_memberships $roles $profiles $perm_hash_array]
    
    return $user_perms
}


ad_proc -public cog_fs_base_path {
	-object_id:required
} {
	Return the base_path for an object_id

	@author malte.sussdorff@cognovis.dealing
	@creation_date 2020-07-29

	@param object_id Object_id for which to return the base_path

	@return base_path if found otherwise empty string
} {

    # get the file path
    set object_type [acs_object_type $object_id]
    switch $object_type {
        im_project {
            set path [im_filestorage_project_path $object_id]
        }
        im_cost - im_invoice - im_trans_invoice {
            set path [im_filestorage_cost_path $object_id]
        }
	    im_company {
            set path [im_filestorage_company_path $object_id]
        }
        user {
            set path [im_filestorage_user_path $object_id]
        }
        default {
            set path ""
        }
    }
    
    return $path
}
