<master>
<property name="title">@page_title;noquote@</property>
<property name="main_navbar_label">finance</property>
<property name="sub_navbar">@sub_navbar;noquote@</property>
  <div style="float: right;">
    <formtemplate id="locale_form">
      <table cellspacing="2" cellpadding="2" border="0">
        <tr class="form-element"><td class="form-label">Language</td>
        <td class="form-widget"><formwidget id="locale"></td></tr>
        @form_vars;noquote@
        <tr class="form-element">
        <td align="left" colspan="2"><formwidget id="formbutton:ok"></td></tr>
      </table>
    </formtemplate>
  </div>
  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td style="background: #CCCCCC">
        <table cellpadding="4" cellspacing="1" border="0">
          <tr style="background: #FFFFe4">
            <th>#intranet-cost.Document_Type#</th>
            <th colspan=2>@default_locale_label@ Message</th>
            <if @default_locale@ ne @current_locale@>
              <th colspan=2>@locale_label@ Message</th>
            </if>
          </tr>
          <tr style="background: #FFFFe4">
            <td></td>
            <td>#acs-mail-lite.Subject#</td>
            <td>#acs-mail-lite.Message#</td>
            <if @default_locale@ ne @current_locale@>
                <td>#acs-mail-lite.Subject#</td>
                <td>#acs-mail-lite.Message#</td>
            </if>
          </tr>
          <multiple name="messages">
            <tr style="background: #EEEEEE">
              <td>
                @messages.category@
              </td>
              <td>@messages.default_subject;noquote@ <p/> <a href='@messages.default_subject_edit_url@'><img src="/shared/images/Edit16.gif" border="0" width="16" height="16"></a>
</td>
              <td>@messages.default_body;noquote@ <p/> <a href='@messages.default_body_edit_url@'><img src="/shared/images/Edit16.gif" border="0" width="16" height="16"></a>
              </td>
              <if @default_locale@ ne @current_locale@>
              <td><if @messages.translated_subject@ ne "">@messages.translated_subject;noquote@ <p/> <a href='@messages.translated_subject_edit_url@'><img src="/shared/images/Edit16.gif" border="0" width="16" height="16"></a></if>
              </td>
                            <td><if @messages.translated_body@ ne "">@messages.translated_body;noquote@ <p/> <a href='@messages.translated_body_edit_url@'><img src="/shared/images/Edit16.gif" border="0" width="16" height="16"></a></if>
                            </td>
              </if>
            </tr>
          </multiple>
        </table>
      </td>
    </tr>
  </table>