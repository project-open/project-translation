ad_library {
    cognovis specific procedures for the whole system
    
    @author malte.sussdorff@cognovis.de
}

ad_proc -public cog::packages_to_upgrade {
	{ -package_key ""}
} {
	Returns a list of package_keys which can be upgraded

	If we provide a package_key we only try to upgrade that one
} {

	#
	# Retrieve all spec files
	#
	set packages_spec_files     [apm_scan_packages "$::acs::rootdir/packages"]
	set workspace_spec_files    [apm_scan_packages [apm_workspace_install_dir]]
	set workspace_filenames     [list]
	foreach spec_path $workspace_spec_files {
		lappend workspace_filenames [file tail $spec_path]
	}
	set all_spec_files $workspace_spec_files
	foreach spec_path $packages_spec_files {
		set spec_filename [file tail $spec_path]
		if {$spec_filename ni $workspace_filenames} {
			lappend all_spec_files $spec_path
		}
	}

	#
	# Parse the files and make a list of available packages to upgrade
	#
	set packages_to_upgrade [list]
	foreach spec_file $all_spec_files {
		array set version    [apm_read_package_info_file $spec_file]
		set this_version     $version(name)
		set this_package_key $version(package.key)
		#
		# Filter by package_key, if passed as an argument, and check for upgrades
		#
		if {($package_key eq "" || $package_key eq $this_package_key) &&
			[apm_package_supports_rdbms_p -package_key $this_package_key] &&
			[apm_package_registered_p $this_package_key] &&
			[apm_package_installed_p $this_package_key] &&
			[apm_higher_version_installed_p $this_package_key $this_version] eq 1
		} {
			#
			# Add the package to the list
			#
			lappend packages_to_upgrade $this_package_key
		}
	}

	return $packages_to_upgrade
}

ad_proc -public cog::run_update_scripts {
    {-package_key ""}
} {
    Runs the upgrade script which havent run yet  for a package

    @param package_key Package we want to run this for, defaults to all
} {

    # Get the list of upgrade scripts in the FS
    set missing_modules [list]
    set core_dir "[acs_root_dir]/packages"

    set where_clause_list [list "enabled_p = 't'"]
    if {$package_key ne ""} {
        lappend where_clause_list "package_key = :package_key"
    }

    set package_sql "
        select distinct
                package_key
        from    apm_package_versions
        where   [join $where_clause_list " and "]
    "
    db_foreach packages $package_sql {
        set core_upgrade_dir "$core_dir/$package_key/sql/postgresql/upgrade"
        foreach dir [lsort [glob -type f -nocomplain "$core_upgrade_dir/upgrade-?.?.?.?.?-?.?.?.?.?.sql"]] {
            # Skip upgrade scripts from 3.0.x
            if {[regexp {upgrade-3\.0.*\.sql} $dir match path]} { continue }

            # Add the "/packages/..." part to hash-array for fast comparison.
            if {[regexp {(/packages.*)} $dir match path]} {
                set fs_files($path) $path
            }
        }
    }


    # --------------------------------------------------------------
    # Get the upgrade scripts that were executed
    set sql "
        select  distinct l.log_key
        from    acs_logs l
        order by log_key
    "
    db_foreach db_files $sql {
        # Add the "/packages/..." part to hash-array for fast comparison.
        if {[regexp {(/packages.*)} $log_key match path]} {
            set db_files($path) $path
        }
    }

    # --------------------------------------------------------------
    # Check if there are scripts that weren't executed:
    set upgrade_list [list]
    set files [lsort [array names fs_files]]
    foreach file $files {
        if {![info exists db_files($file)]} {
            lappend upgrade_list $file
            db_source_sql_file -callback apm_dummy_callback "[acs_root_dir]$file"
        }
    }
    return $upgrade_list
}


ad_proc -public cog::update_package {
    -package_key:required
} {
    Update a package to the latest version
} {

    # Are there packages to upgrade?
    set packages_to_upgrade [cog::packages_to_upgrade -package_key $package_key]
    
    # Dependency check
    apm_get_package_repository -array repository
    apm_get_installed_versions -array installed

    array set result [apm_dependency_check_new \
                        -repository_array repository \
                        -package_keys $packages_to_upgrade]

    set packages_upgraded [list]

    if {$result(status) eq "ok"} {
        #
        # Do the upgrade
        #
        foreach package_key $result(install) {
            #
            # As we may have new packages included by the dependency check,
            # determine if we are upgrading or installing.
            #
            set spec_file       [apm_package_info_file_path $package_key]
            array set version   [apm_read_package_info_file $spec_file]
            set new_version     $version(name)
            set package_name $version(package-name)

            # Check on the installed version vs. new install (empty installed)
            if { [apm_package_upgrade_p $package_key $new_version] == 1} {
                set installed_version [apm_highest_version_name $package_key]
            } else {
                set installed_version ""
            }

            #
            # Select SQL scripts
            #
            set data_model_files [apm_data_model_scripts_find \
                                    -upgrade_from_version_name $installed_version \
                                    -upgrade_to_version_name $new_version \
                                    $package_key]
        
            # Upgrade the package
            if {[catch {
                set version_id [apm_package_install \
                        -enable=1 \
                        -load_data_model \
                        -data_model_files $data_model_files \
                        $spec_file]

            } errorMsg]} {
                cog_log Error "cog_rest::update_system : $package_key $errorMsg\n [ns_quotehtml $::errorInfo]"
            } else {
                lappend packages_upgraded $package_key
            }
        }
        return $packages_upgraded
    } else {
        return 0
    }
}



ad_proc -public cog::anonymize_data {
    -translation:boolean
} {
    Anonymize the data in the system
} {

    db_dml disable_trigger "alter table im_companies disable trigger im_companies_tsearch_tr"
    db_dml disable_trigger "alter table persons disable trigger persons_tsearch_tr"
    db_dml anonymize {
        update acs_events set description =  translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaabbawiwiqihhasdgkksskjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ'), name = translate(name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ'), related_link_text = translate(related_link_text, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ');
        update acs_mail_log set subject = translate(subject, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        update acs_mail_log set body = translate(body, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        update cr_revisions set content = translate(content, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), title = translate(title, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        update cr_items set name = item_id || ' - ' || translate(name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        update acs_objects set title = translate(title, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        update im_forum_topics set message = translate(message, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), subject = translate(subject, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        update im_costs set cost_name = translate(cost_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');        update im_notes set note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        update users set username = translate(username, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ');

        alter table im_projects disable trigger ALL;
        update im_projects set company_project_nr =  translate(company_project_nr, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),  
            description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),
            project_name = translate(project_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),
            project_path = translate(project_path, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ'),
            final_company = translate(final_company, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),
            note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ')
        ;
        alter table im_projects enable trigger ALL;
        
        alter table im_trans_tasks disable trigger ALL;
        update im_trans_tasks set task_name =  translate(task_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKJJJJUUQQOOIIPPPPZZFFTZZ'), 
            description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),
            task_filename = translate(task_filename, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        alter table im_trans_tasks enable trigger ALL;
        
        update parties set email = translate(email, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ'),
            signature = translate(signature, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),
            url = translate(url, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');

        
        alter table persons disable trigger ALL;
        update persons set first_names = translate(first_names, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ'),
            last_name = translate(last_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ'),
            bio = translate(bio, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ'),
            position = translate(position, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ');
        alter table persons enable trigger ALL;

    }

    if {$translation_p} {
        db_dml anonymize_translation {
            update im_freelance_packages set freelance_package_name = translate(freelance_package_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ'), package_comment = translate(package_comment, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ');
            update im_freelance_assignments set assignment_name = translate(assignment_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), assignment_comment = translate(assignment_comment, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
            update im_assignment_quality_reports set comment = translate(comment, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
            update im_freelance_package_files set freelance_package_file_name =  translate(freelance_package_file_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),  file_path = translate(file_path, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        

        }
    }

    #---------------------------------------------------------------
    # Companies
    #---------------------------------------------------------------
    set internal_company_id [im_company_internal]

    db_dml companies {
        alter table im_companies disable trigger ALL;
        update im_companies set
			company_name=translate(company_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ'),
			company_path=translate(company_path, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ'),
			referral_source=translate(referral_source, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ'),
			site_concept=translate(site_concept, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ'),
			vat_number=translate(vat_number, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ'),
			note=translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ');
        alter table im_companies enable trigger ALL;
    }    
    if [ im_column_exists im_companies bank_account_nr] {
        db_dml company_bank {
            alter table im_companies disable trigger ALL;
                update im_companies set
                bank_account_nr=translate(bank_account_nr, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
                bank_routing_nr=translate(bank_routing_nr, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
                bank_name=translate(bank_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ'),
                iban=translate(iban, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
                bic=translate(bic, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ');
            alter table im_companies enable trigger ALL;
        }
    }

    if [ im_column_exists im_companies paypal_email] {
        db_dml company_bank {
            alter table im_companies disable trigger ALL;
            update im_companies set
                paypal_email=translate(paypal_email, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577');
            alter table im_companies enable trigger ALL;
        }
    }

    if [ im_column_exists im_companies skrill_email] {
        db_dml company_bank {
            alter table im_companies disable trigger ALL;
            update im_companies set
                skrill_email=translate(skrill_email, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577');
            alter table im_companies enable trigger ALL;
        }
    }

    db_dml update_internal "update company_path = 'internal' where company_id = :internal_company_id"

    # ---------------------- im_invoice_items -------------------------------
    set im_invoice_items_sql "
            select	item_id,
            item_name,
            price_per_unit,
            description
        from	im_invoice_items
    "

    db_foreach im_invoice_items_select $im_invoice_items_sql {
        set new_price "0.[expr round(100*rand())]"
        db_dml im_invoice_items_update "
        update im_invoice_items set
            item_name = translate(item_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ')
            price_per_unit = :new_price
        where item_id = :item_id
        "
    }


    # ---------------------- im_trans_prices -------------------------------
    if {[im_table_exists im_trans_prices]} {
        set im_trans_prices_sql "
            select	price_id
        from	im_trans_prices
        "
        db_foreach im_prices_select $im_trans_prices_sql {
            set new_price [expr round(100*rand()) / 100]
            db_dml im_trans_prices_update "
                update im_trans_prices set
                    price = :new_price,
                    note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ')
                where price_id = :price_id
            "
        }
    }



    db_dml im_offices_update {
		update im_offices set
			office_name = translate(office_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			office_path = translate(office_path, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			phone = translate(phone, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			fax = translate(fax, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			address_line1 = translate(address_line1, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			address_line2 = translate(address_line2, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			address_city = translate(address_city, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			address_state =translate(address_state, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			address_postal_code = translate(address_postal_code, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			landlord = translate(landlord, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			security = translate(security, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577')
    }

    db_dml users_contact_update {
		update users_contact set
			home_phone = translate(home_phone, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			work_phone = translate(work_phone, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			cell_phone =translate(cell_phone, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			pager = translate(pager, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			fax = translate(fax, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			aim_screen_name = translate(aim_screen_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			icq_number = translate(icq_number, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			ha_line1 = translate(ha_line1, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			ha_line2 = translate(ha_line2, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			ha_city = translate(ha_city, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			ha_state = translate(ha_state, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			ha_postal_code = translate(ha_postal_code, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			wa_line1 = translate(wa_line1, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			wa_line2 = translate(wa_line2, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			wa_city = translate(wa_city, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			wa_state = translate(wa_state, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			wa_postal_code = translate(wa_postal_code, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			current_information = translate(current_information, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577')
    }

    db_dml users_email_update {
        update users_email set
            smtpuser = translate(smtpuser, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
            smtppassword = translate(smtppassword, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
            smtphost = translate(smtphost, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
            imaphost = translate(imaphost, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
            imapuser = translate(imapuser, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
            imappassword =translate(imappassword, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577')
    }

    db_dml im_user_absences_update {
		update im_user_absences set
			absence_name = translate(absence_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			contact_info = translate(contact_info, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577')
    }

    db_dml im_employees_update {
		update im_employees set
			personnel_number =translate(personnel_number, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			ss_number = translate(ss_number, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			educational_history = translate(educational_history, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			termination_reason = translate(termination_reason, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			last_degree_completed =translate(last_degree_completed, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			skills = translate(skills, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577')
    }


    db_dml im_cost_centers_update {
		update im_cost_centers set
			cost_center_name = translate(cost_center_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			cost_center_label = translate(cost_center_label, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577'),
			description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ4574574577')
    }
}


ad_proc -public im_sysconfig_change_server { 
    -server_path
    -server_url
    -system_owner
    -develop:boolean
} {
    Allows moving a server to a different location. This will change the most typical parameters when you copy e.g. the database from production to staging
} {

    parameter::set_from_package_key -package_key acs-kernel -parameter "SystemURL" -value $server_url
	parameter::set_from_package_key -package_key intranet-core -parameter "UtilCurrentLocationRedirect" -value $server_url
	
    parameter::set_from_package_key -package_key acs-kernel -parameter "SystemOwner" -value $system_owner

    parameter::set_from_package_key -package_key intranet-core -parameter "BackupBasePathUnix" -value "${server_path}/filestorage/backup"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "BugBasePathUnix" -value "${server_path}/filestorage/bugs"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "CompanyBasePathUnix" -value "${server_path}/filestorage/companies"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "HomeBasePathUnix" -value "${server_path}/filestorage/home"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "ProjectBasePathUnix" -value "${server_path}/filestorage/projects"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "ProjectSalesBasePathUnix" -value "${server_path}/filestorage/project_sales"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "TicketBasePathUnix" -value "${server_path}/filestorage/tickets"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "UserBasePathUnix" -value "${server_path}/filestorage/users"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "CostBasePathUnix" -value "${server_path}/filestorage/costs"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "EventBasePathUnix" -value "${server_path}/filestorage/events"
    parameter::set_from_package_key -package_key intranet-invoices -parameter "InvoiceTemplatePathUnix" -value "${server_path}/filestorage/templates"
    catch {parameter::set_from_package_key -package_key intranet-mail-import -parameter "MailDir" -value "${server_path}/maildir"}
    
    # Set parameters for redirecting mail
    if {$develop_p} {
		parameter::set_from_package_key -package_key acs-mail-lite -parameter "EmailDeliveryMode" -value "redirect"
		parameter::set_from_package_key -package_key acs-mail-lite -parameter "EmailRedirectTo" -value "$system_owner"
		parameter::set_from_package_key -package_key intranet-core -parameter "TestDemoDevServer" -value "1"
	if {[apm_package_installed_p xotcl-core]} {
	    parameter::set_from_package_key -package_key xotcl-core -parameter "NslogRedirector" -value "1"
	}
		parameter::set_from_package_key -package_key intranet-core -parameter "TestDemoDevServer" -value "1"
		if {[apm_package_installed_p intranet-collmex]} {
    		parameter::set_from_package_key -package_key intranet-collmex -parameter "ActiveP" -value "0"	    
    	}
    }
    
    if {[apm_package_installed_p intranet-collmex]} {
		parameter::set_from_package_key -package_key intranet-collmex -parameter "Login" -value ""	    
    }
}