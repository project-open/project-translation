ad_page_contract {
    Export messages from the database to catalog files.

    @author Peter Marklund
} {
    {locale:word,multiple ""}
    {package_key:token ""}
    {return_url:localurl "/acs-lang/admin"}
}

set page_title "Export messages"

if { ![acs_user::site_wide_admin_p] } {
    ad_return_warning "Permission denied" "Sorry, only site-wide administrators are allowed to export messages from the database to catalog files."
    ad_script_abort
}

lang::catalog::export \
    -package_key $package_key \
    -locales $locale

set catalog_file_path [lang::catalog::get_catalog_file_path  -package_key $package_key  -locale $locale]

set outputheaders [ns_conn outputheaders]
ns_set cput $outputheaders "Content-Disposition" "attachment; filename=[file tail $catalog_file_path]"

ns_returnfile 200 text/html $catalog_file_path