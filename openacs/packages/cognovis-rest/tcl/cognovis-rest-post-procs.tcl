ad_library {
	@author malte.sussdorff@cognovis.de
}

# --------------------------------------------------------
# POST on the object type - CREATE
# --------------------------------------------------------
namespace eval cog_rest::json_object {
	ad_proc mail {} {
        @return subject string Subject Line for the mail
        @return body string HTML for the body of the mail already pre-filled.
        @return recipient named_id Recipient of the mail (contact)
        @return sender named_id Sender of the email (typically the project_lead)
        @return invoice_revision cr_file Revision of an attached file
	} -

	ad_proc mail_body {} {
		@param from_party_id object party::read Sender for the E-mail
		@param to_party_ids object_array party::read Recipients, might be empty in case we use to_addr instead
		@param to_addr string E-Mail address to whom we send the mail to (instead of using the party_id)
		@param cc_addr string CC E-Mail addresses
		@param subject string Subject for the E-Mail
		@param body string Body for the E-Mail
		@param context_id integer object_id which is related to this E-Mail
		@param files string List of files
	} -

	ad_proc i18n_body {} {
		@param message_key string Message Key we want to create
		@param package_key string Package in which we want to create the key
		@param message string Message we want to Store
		@param locale string Locale for which we want to register the message
	} -
}

namespace eval cog_rest::post {

	ad_proc -public send_mail {
		-from_party_id:required
		{ -to_party_ids "" }
		{ -to_addr "" }
		{ -cc_addr "" }
		{ -subject "" }
		{ -body "" }
		{ -context_id ""}
		{ -files "" }

	} {
		Sends a mail

		@param mail_body request_body Mail object

		@return message json_object Object with the message_id
		@return_message message_id string Id of the E-Mail send
	} {
		if {$to_addr eq "" && $to_party_ids eq ""} {
			cog_rest::error -http_status 400 -message "You need to provide at least one recipient, otherwise we can't send the E-Mail."
			return
		}

		# Get the file revisions if we have a context,  otherwise we could not find it
		set file_revision_ids [list]
		if {$context_id ne ""} {
			foreach file [split $files ","] {
				set file_item_id [content::item::get_id_by_name -name $file -parent_id $context_id]
				if {$file_item_id ne ""} {
					lappend file_revision_ids [content::item::get_best_revision -item_id $file_item_id]
				}
			}
		}
		set message_id [intranet_chilkat::send_mail -from_party_id $from_party_id -to_party_ids $to_party_ids -to_addr $to_addr -cc_addr $cc_addr -subject $subject -body $body -object_id $context_id -file_ids $file_revision_ids]
		return [cog_rest::json_object]
	}

	ad_proc -public i18n {
		-message_key:required
		-package_key:required
		{ -message "" }
		{ -locale "" }
		-rest_user_id:required   
	} {
		Registers a message key

		@param i18n_body request_body Message information we want to store

		@return translation json_object translation
	} {
		if {$message eq ""} {
			regsub -all {_} $message_key { } message
		}

		if {$locale eq ""} {
			set locale "en_US"
		}

		lang::message::register $locale $package_key $message_key $message

		set translation [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::i18n -message_key $message_key -package_key $package_key -locale $locale -rest_user_id $rest_user_id]]
		return [cog_rest::json_response]
	}
}

