# /packages/intranet-invoices/www/invoice-reminder.tcl
#


ad_page_contract {
    Purpose: Send a reminder to all invoice recipients

    @param return_url the url to return to
    @author malte.sussdorff@cognovis.de
} {
    { return_url "/intranet-invoices/" }
    { cost:multiple "" }
    { cost_status:array "" }
    { new_payment_amount:array "" }
    { new_payment_currency:array "" }
    { new_payment_date:array "" }
    { new_payment_type_id:array "" }
    { new_payment_company_id:array "" }
    { invoice_action "" }

}

set user_id [auth::require_login]
if {![im_permission $user_id add_invoices]} {
    ad_return_complaint 1 "<li>You have insufficient privileges to see this page"
    return
}


foreach cost_id $cost {
	
	db_1row invoice_info "select company_contact_id as recipient_id, customer_id as company_id, email, invoice_nr, effective_date, now() - interval '3 days' as payment_date, now() + interval '10 days' as reminder_date, now()::date - effective_date::date - payment_days as overdue_days from im_invoices, parties, im_costs where party_id = company_contact_id and invoice_id = :cost_id and invoice_id = cost_id"
	
	# Nichts tun wenn überfällige Tage < 1
	if {$overdue_days > 0} {
	
		# Get the project information of the first linked project
		db_1row related_projects_sql "
				select distinct
			   	r.object_id_one as project_id,
				p.project_name,
				first_names || ' ' || last_name as project_lead,
				pa.email as project_lead_email,
				p.project_nr,
				p.parent_id,
				p.description,
				trim(both p.company_project_nr) as customer_project_nr
			from
				acs_rels r,
				im_projects p,
				persons pe,
				parties pa
			where
				r.object_id_one = p.project_id
				and r.object_id_two = :cost_id
				and p.project_lead_id = pe.person_id
				and pa.party_id = pe.person_id
				limit 1
		"
		
		#	set recipient_locale [lang::user::locale -user_id $recipient_id]
		set recipient_locale "de_DE"	
		set effective_date_pretty [lc_time_fmt $effective_date %q $recipient_locale]
		set payment_date_pretty [lc_time_fmt $payment_date %q $recipient_locale]
		set reminder_date_pretty [lc_time_fmt $reminder_date %q $recipient_locale]
			
		# We must have generated the invoice before, therefore we won't generate it again	
		set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $cost_id]
		set invoice_revision_id [content::item::get_best_revision -item_id $invoice_item_id]
		
		set subject [lang::util::localize "#intranet-invoices.invoice_reminder_subject#" $recipient_locale]
		set body [lang::util::localize "#intranet-invoices.invoice_reminder_body#" $recipient_locale]
		if {![ad_looks_like_html_p $body]} {
			set body [ad_text_to_html $body]
		}
		
		intranet_chilkat::send_mail \
			-to_addr $project_lead_email \
			-from_addr "$project_lead_email" \
			-subject "$subject" \
			-body "$body" \
			-file_ids $invoice_revision_id \
			-object_id $cost_id

		set invoice_reminder_status_id [parameter::get_from_package_key -package_key "intranet-invoices" -parameter InvoiceReminderStatusID -default ""]
		if {$invoice_reminder_status_id ne ""} {
			db_dml update_status "update im_costs set cost_status_id = :invoice_reminder_status_id where cost_id = :cost_id"
		}
		
		callback intranet-invoices::reminder_send -invoice_id $cost_id
	}
}

ad_returnredirect $return_url


