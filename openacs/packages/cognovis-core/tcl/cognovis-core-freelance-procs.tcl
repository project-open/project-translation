namespace eval cog::freelance {
    ad_proc skill_component {
        -current_user_id:required
        -user_id:required
        -return_url:required
    } {
        Returns the skill component for Freelancers

        @current_user_id User who is currently viewing the skills - used for I18N
        @user_id User for whom we look at the skills
        @return_url URL we should return to
    } {

        set view 0
        set read 0
        set write 0
        set admin 0
        im_user_permissions $current_user_id $user_id view read write admin
        if {!$read} { 
            return "" 
        }

        # Skip this component if the user is not a freelancer
        # if {![im_profile::member_p -profile "Freelancers" -user_id $user_id]} { 
        # return "" 
        # }

        # Check permissions to see and modify freelance skills and their confirmations
        #
        set view_freelance_skills_p [im_permission $current_user_id view_freelance_skills]
        set add_freelance_skills_p [im_permission $current_user_id add_freelance_skills]
        set view_freelance_skillconfs_p [im_permission $current_user_id view_freelance_skillconfs]
        set add_freelance_skillconfs_p [im_permission $current_user_id add_freelance_skillconfs]

        set sql "
            select
                sk.skill_id,
                im_category_from_id(sk.skill_id) as skill,
                c.category_id as skill_type_id,
                im_category_from_id(c.category_id) as skill_type,
                im_category_from_id(sk.claimed_experience_id) as claimed,
                im_category_from_id(sk.confirmed_experience_id) as confirmed,
                sk.claimed_experience_id,
                sk.confirmed_experience_id
            from
                (select c.*
                    from im_categories c
                    where c.category_type = 'Intranet Skill Type'
                    and enabled_p = 't'
                    order by c.category_id
                ) c left outer join
                (select *
                    from im_freelance_skills
                    where user_id = :user_id
                    order by skill_type_id
                ) sk
            on
                sk.skill_type_id = c.category_id
            order by
                c.category_id
        "

        # ------------- Freelance Skill Table Header -------------------------------
        set ctr 1
        set old_skill_type_id 0
        set skill_header_titles ""
        db_foreach column_list $sql {
            if {$old_skill_type_id != $skill_type_id} {
                set admin_html ""
                append skill_header_titles "
                <td align=center>
                <b>[lang::message::lookup [lang::user::locale] intranet-core.[lang::util::suggest_key $skill_type] $skill_type] $admin_html</b>
                </td>"
                set old_skill_type_id $skill_type_id
            }
            incr ctr
        }
        set colspan $ctr

        
        set admin_url [export_vars -base "/intranet/admin/categories/index" {{select_category_type "Intranet Skill Type"}}]
        set admin_html "<a href=\"$admin_url\">[im_gif wrench]</a>"
        if {![im_is_user_site_wide_or_intranet_admin $current_user_id]} { set admin_html "" }

        set skill_header_html "
            <table cellpadding=0 cellspacing=2 border=0>
            <tr>
            <td class=rowtitle align=center colspan=$colspan>[_ intranet-freelance.Skills] $admin_html</td>
            </tr>
            <tr class=rowtitle>
            $skill_header_titles
            </tr>\n
        "


        # ------------- Freelance Skill Table Body -------------------------------
        # A horizontal array of tables, each representing freelance skills

        # Setup the horizontal table start
        #
        set skill_body_html "
            <tr valign=top class=rowodd>
            <td>
                <table cellpadding=0 cellspacing=1 border=1 width=100%>
                <tr class=roweven>
                <td>Skill</td>
                <td align=center>[_ intranet-freelance.Claim]</td>
                </tr>
        "

        set old_skill_type_id 0
        set primera 1
        set ctr 1

        # I make a comparation between Claimed and Confirmed
        # to generate a "tick" or not if confirmed is correct.
        db_foreach skill_body_html $sql {

            if {$primera == 1} { 
                set old_skill_type_id $skill_type_id
                set primera 0
            }

            if {$old_skill_type_id != $skill_type_id} {
                append skill_body_html "
            </table>
            </td>
            <td>
            <table cellpadding=0 cellspacing=0 border=1 width=100%>
                <tr class=roweven>
                <td>[_ intranet-freelance.Skill]</td>
                <td align=center>[_ intranet-freelance.Claim]</td>
                </tr>"
                set old_skill_type_id $skill_type_id
                set ctr 1
            }

            # Display a tick or a cross, depending whether the claimed
            # experience is confirmed or not.
            #
            set confirmation ""
            if {$view_freelance_skillconfs_p} {
                if {"" != $confirmed && ![string equal "Unconfirmed" $confirmed]} {
                if {$claimed_experience_id <= $confirmed_experience_id } {
                    set confirmation [im_gif tick]
                } else {
                    set confirmation [im_gif wrong]
                }
                }
            }
            set experiences_html_eval "<td align=left>$claimed$confirmation</td></tr>\n\t"

            
            if {[string equal "" $skill]} {
                append skill_body_html ""
            } else {
                append skill_body_html "<tr><td>[lang::message::lookup [lang::user::locale] intranet-core.[lang::util::suggest_key $skill] $skill]</td>"
                append skill_body_html "$experiences_html_eval"
            }
            incr ctr
        }
        append skill_body_html "</table></td></tr>\n\t"
        
        if { $write } {
            # ------------  we put buttons for each skill for change its.
            
            set language_buttons_html "<tr align=center>"
            set old_skill_type_id 0
            db_foreach column_list $sql {
                if {$old_skill_type_id != $skill_type_id} {
                append language_buttons_html "
        <td><form method=POST action=/intranet-freelance/skill-edit>
        [export_vars -form {user_id skill_type_id return_url}]
        <input type=submit value=Edit></form></td>"
                set old_skill_type_id $skill_type_id
                }
            }
        } else {
           set language_buttons_html ""
        }

        append language_buttons_html "</tr>\n\t"
        append languages_html "
            $skill_header_html\n
            $skill_body_html\n
            $language_buttons_html
            </table>
        "

        return $languages_html
    }
}