ad_library {

    Initialization for cognovis-core module
    
    @author malte.sussdorff@cognovis.de
}

ns_logctl severity Error -color red

# Initialize the search "semaphore" to 0.
# There should be only one thread indexing files at a time...
nsv_set intranet_timesheet2 timesheet_synchronizer_p 0

# Check for imports of external im_hours entries every every X minutes
ad_schedule_proc -thread t 3600 im_timesheet2_sync_timesheet_costs

# Schedule the reminders
set remind_employees_p [parameter::get_from_package_key -package_key cognovis-core -parameter RemindEmployeesToLogHoursP -default 0 ]

if {$remind_employees_p} {
    ad_schedule_proc -thread t -schedule_proc ns_schedule_weekly [list 1 7 0] im_timesheet_remind_employees
}

ad_proc -public -callback im_category_after_update {
    {-object_id:required}
    {-type ""}
    {-status ""}
    {-category_id ""}
    {-category_type ""}
} {
    This is a callback to map attributes and categories using respectively attribute_id and category_id

    @param category_id ID of the category
    @param category_type Type of the category
} -
