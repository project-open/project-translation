-- 
-- 
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2012-04-07
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-freelance/sql/postgresql/upgrade/upgrade-4.1.0.0.3-4.1.0.0.4.sql','');


SELECT im_component_plugin__new (
    null,					-- plugin_id
    'acs_object',				-- object_type
    now(),					-- creation_date
    null,					-- creation_user
    null,					-- creation_ip
    null,					-- context_id
    'User Company Info',		-- plugin_name
    'intranet-freelance',	-- package_name
    'right',					-- location
    '/intranet/users/view',		-- page_url
    null,					-- view_name
    1,					-- sort_order
    'im_freelance_company_info_component $user_id $return_url',
    'lang::message::lookup "" intranet-freelance.Company_Info "User Company Info"'
);


select acs_permission__grant_permission(
    plugin_id,
    (select group_id from groups where group_name = 'Employees'),
    'read')
from im_component_plugins
where plugin_name in ('User Company Info')
and package_name = 'intranet-freelance';

select acs_permission__grant_permission(
    plugin_id,
(select group_id from groups where group_name = 'Freelancers'),
    'read')
from im_component_plugins
where plugin_name in ('User Company Info')
and package_name = 'intranet-freelance';


SELECT im_component_plugin__new (
    null,					-- plugin_id
    'acs_object',				-- object_type
    now(),					-- creation_date
    null,					-- creation_user
    null,					-- creation_ip
    null,					-- context_id
    'User Price Info',		-- plugin_name
    'intranet-freelance',	-- package_name
    'right',					-- location
    '/intranet/users/view',		-- page_url
    null,					-- view_name
    3,					-- sort_order
    'im_freelance_company_price_component $current_user_id $user_id $return_url',
    'lang::message::lookup "" intranet-freelance.Company_Price_Info "User Price Info"'
);


select acs_permission__grant_permission(
    plugin_id,
    (select group_id from groups where group_name = 'Employees'),
    'read')
from im_component_plugins
where plugin_name in ('User Price Info')
and package_name = 'intranet-freelance';

