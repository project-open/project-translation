set title "Endpoints"
set endpoints_html ""
foreach end_point [lsort [cog_rest::endpoints]] {
	foreach operation [list get post] {
		set custom_proc [cog_rest::endpoint_proc -endpoint $end_point -operation $operation]

		if {$custom_proc ne ""} {
			array set doc_elements [nsv_get api_proc_doc $custom_proc]
			array set flags $doc_elements(flags)
			array set default_values $doc_elements(default_values)
			
			set endpoint_url [export_vars -base "/api-doc/proc-view" -url {{proc $custom_proc}}]
			set out "<dt><b><a href=$endpoint_url>$end_point</a></b></dt><dd><dl>\n"
			set switch_exists_p 0
			foreach switch $doc_elements(switches) {
				switch $switch {
					format - rest_user_id - rest_otype - rest_oid  - debug {
						# do nothing
					}
					default {
						set switch_exists_p 1
						append out "<dt><b>-$switch</b>"
						if { [lsearch $flags($switch) "boolean"] >= 0 } {
							append out " (boolean)"
						}
					
						if { [info exists default_values($switch)] && $default_values($switch) ne "" } {
							append out " (defaults to <code>\"$default_values($switch)\"</code>)"
						}
					
						if { [lsearch $flags($switch) "required"] >= 0 } {
							append out " (required)"
						} else {
							append out " (optional)"
						}
						append out "</dt>"
						if { [info exists params($switch)] } {
							append out "<dd>$params($switch)</dd>"
						}
					}
				}
			}
		}
	}
	if {!$switch_exists_p} {
		append out "<dt>Missing proper documentation in <a href=\"/api-doc/procs-file-view?path=[ns_urlencode $doc_elements(script)]\"> $doc_elements(script)</a>. Go and document me</dt>"
	}
	append out "</dl></dd>\n"
	append endpoints_html "$out"
}