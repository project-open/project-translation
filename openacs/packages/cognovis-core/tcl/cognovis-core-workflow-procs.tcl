ad_library {
    Procedures handling workflows
    @author malte.sussdorff@cognovis.de
}

ad_proc wf_trace_column_change__begin {
    {-trace_array:required ""}
    {-object_type_id:required ""}
    {-object_type:required ""}
    {-table:required ""}
    {-where_clause:required ""}
    {-column_array:required ""}
} {
    @author Neophytos Demetriou
} {
    upvar $trace_array wf_trace_cols
    upvar $column_array old

    set dynfields \
        [im_dynfield::dynfields \
            -object_type_id $object_type_id \
            -object_type $object_type]

    foreach attribute_id $dynfields {
         set column_name [im_dynfield::attribute::get_name_from_id -attribute_id $attribute_id]
         set pretty_name $column_name
         set proc_name ""
         if { [string match {*_id} $column_name] } {
             set proc_name "im_name_from_id"
         }
         set wf_trace_cols($column_name) [list $pretty_name $proc_name]
    }

    # start_date, 
    # end_date,
    # absence_type_id,
    # vacation_replacement_id,
    set where_clause [uplevel [list db_bind_var_substitution $where_clause]]
    set sql "
        select [join [array names wf_trace_cols] {,}]
        from im_user_absences 
        where ${where_clause}
    "
    db_1row old_data $sql -column_array old

}


ad_proc wf_trace_column_change__end {
    {-user_id:required ""}
    {-object_id:required ""}
    {-trace_array:required ""}
    {-table:required ""}
    {-where_clause:required ""}
    {-column_array:required ""}
    {-what "record"}
} {
    @author Neophytos Demetriou
} {
    upvar $trace_array wf_trace_cols
    upvar $column_array old

    # start_date, 
    # end_date,
    # absence_type_id,
    # vacation_replacement_id,
    set where_clause [uplevel [list db_bind_var_substitution $where_clause]]
    set sql "
        select [join [array names wf_trace_cols] {,}]
        from im_user_absences 
        where ${where_clause}
    "
    db_1row old_data $sql -column_array new

    set message ""
    foreach {column_name column_def} [array get wf_trace_cols] {
        foreach {pretty_name proc_name} $column_def break

        if { $old($column_name) ne $new($column_name) } {
            
            if { $proc_name ne {} } {
                append message "$pretty_name changed from [$proc_name $old($column_name)] to [$proc_name $new($column_name)]"
            } else {
                append message "$pretty_name changed from $old($column_name) to $new($column_name)"
            }

            callback im_trace_column_change \
                -user_id $user_id \
                -object_id $object_id \
                -table $table \
                -column_name $column_name \
                -pretty_name $pretty_name \
                -old_value $old($column_name) \
                -new_value $new($column_name)

        }

    }

    if {$message ne {}} {
        set message "[im_name_from_user_id $user_id] modified the ${what}. ${message}"
        callback im_trace_table_change \
            -object_id $object_id \
            -table $table \
            -message $message
    }

}

ad_proc -callback im_trace_table_change -impl im_trace_absence_change {
    -object_id
    -table
    -message 
} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {

    if {$table ne {im_user_absences}} {
        return
    }

    set action "modify absence"
    set action_pretty "Modify Absence"

    set case_id [db_string get_case "select min(case_id) from wf_cases where object_id = :object_id"]

    im_workflow_new_journal \
        -case_id $case_id \
        -action $action \
        -action_pretty $action_pretty \
        -message $message
}

namespace eval cog::workflow {

    ad_proc -public object_permissions {
        -object_id:required
        -perm_table:required
        {-user_id ""}
    } {
        Determines whether a user can execute the specified "perm_letter" (i.e. r=read, w=write, d=delete) operation on the object. Returns the list of permissions. 

        Fixes bugs from im_workflow_object_permissions
    } {
        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }

        # stuff permission from table into hash
        array set perm_hash $perm_table

        # ------------------------------------------------------
        # Pull out the relevant variables
        set user_id [ad_conn user_id]
        set owner_id [db_string owner "select creation_user from acs_objects where object_id = $object_id" -default 0]
        if {"" == $owner_id} { set owner_id 0 }
        set status_id [db_string status "select im_biz_object__get_status_id (:object_id)" -default 0]
        set type_id [db_string status "select im_biz_object__get_type_id (:object_id)" -default 0]
        set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
        set user_is_hr_p [im_user_is_hr_p $user_id]
        set user_is_accounting_p [im_user_is_accounting_p $user_id]
        set user_is_owner_p [expr {$owner_id == $user_id}]
        set user_is_assignee_p [db_string assignee_p "
    	select	count(*)
    	from	(select	pamm.member_id
    		from	wf_cases wfc,
    			wf_tasks wft,
    			wf_task_assignments wfta,
    			party_approved_member_map pamm
    		where	wfc.object_id = :object_id
    			and wft.case_id = wfc.case_id
    			and wft.state in ('enabled', 'started')
    			and wft.task_id = wfta.task_id
    			and wfta.party_id = pamm.party_id
    			and pamm.party_id = :user_id
    		) t
        "]
        
        if {0 == $status_id} {
            cog_log Error "<b>Invalid Configuration</b>:<br>The PL/SQL function 'im_biz_object__get_status_id (:object_id)' has returned an invalid status_id for object #$object_id.  "
            return ""
        }

        # ------------------------------------------------------
        # Calculate permissions
        set perm_set {}

        if {$user_is_owner_p} { 
        	set perm_letters {}
    	    if {[info exists perm_hash(owner-$status_id)]} { 
                set perm_letters $perm_hash(owner-$status_id)
            }
            set perm_set [set_union $perm_set $perm_letters]
        }
     
        if {$user_is_assignee_p} { 
            set perm_letters {}
            if {[info exists perm_hash(assignee-$status_id)]} { 
                set perm_letters $perm_hash(assignee-$status_id)
            }
            set perm_set [set_union $perm_set $perm_letters]
        }

        if {$user_is_hr_p} { 
            set perm_letters {}
            if {[info exists perm_hash(hr-$status_id)]} { 
                set perm_letters $perm_hash(hr-$status_id)
            }
            set perm_set [set_union $perm_set $perm_letters]
        }

        if {$user_is_accounting_p} { 
            set perm_letters {}
            if {[info exists perm_hash(accounting-$status_id)]} { 
                set perm_letters $perm_hash(accounting-$status_id)
            }
            set perm_set [set_union $perm_set $perm_letters]
        }

        # Admins can do everything anytime.
        if {$user_is_admin_p} { 
            set perm_set {v r w d a} 
        }

        return $perm_set
    }
}