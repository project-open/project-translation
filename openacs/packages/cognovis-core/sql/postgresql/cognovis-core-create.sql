--
--
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2020-11-09
-- @cvs-id $Id$
--

-- Add symbols
update currency_codes set symbol = '€' where iso = 'EUR';
update currency_codes set symbol = '$' where iso = 'USD';

-- Remove unique constraint on im_companies name
create or replace function inline_0 ()
returns integer as $body$
declare
        v_count                 integer;
begin
    SELECT count(*) into v_count
       FROM pg_catalog.pg_constraint con
            INNER JOIN pg_catalog.pg_class rel
                       ON rel.oid = con.conrelid
            INNER JOIN pg_catalog.pg_namespace nsp
                       ON nsp.oid = connamespace
       WHERE rel.relname = 'im_companies' and con.conname = 'im_companies_name_un';
    if v_count = 1 then
        alter table im_companies drop constraint im_companies_name_un; 
    end if;
    return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();    


create or replace function im_menu__delete_by_name (varchar) 
returns integer as $body$
DECLARE
    p_menu_name  alias for $1;
    p_menu_id integer;
BEGIN
    -- First find out the ID
    select menu_id from im_menus where name = p_menu_name into p_menu_id;
    
    -- Erase the im_menus item associated with the id
    delete from     im_menus
    where       menu_id = p_menu_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where       object_id = p_menu_id;
    
    PERFORM acs_object__delete(p_menu_id);
    return 0;
end;$body$ language 'plpgsql';


create or replace function create_menu (varchar, varchar, varchar, varchar, integer, varchar, varchar, varchar, varchar, integer)
    returns integer as $body$

    DECLARE

        p_package_name          alias for $1;
        p_label            alias for $2;
        p_name           alias for $3;
        p_url           alias for $4;
        p_sort_order            alias for $5;
        p_parent_menu_label         alias for $6;
        p_visible_tcl          alias for $7;
        p_menu_gif_small alias for $8;
        p_enabled_p alias for $9;
        p_visible_profile alias for $10;

        v_menu_id integer;
        v_count  integer;
        v_result integer;

    BEGIN
    select menu_id into v_menu_id from im_menus
        where name = p_name;
    IF
    v_menu_id is null
    THEN
        select im_menu__new (
                null,              -- p_menu_id
                'acs_object',  -- object_type
                now(),            -- creation_date
                null,              -- creation_user
                null,              -- creation_ip
                null,              -- context_id
                p_package_name,     -- package_name
                p_label, -- label
                p_name,               -- name
                p_url, -- url
                p_sort_order,                  -- sort_order
                (select menu_id from im_menus where label = p_parent_menu_label),  -- parent_menu_id
                p_visible_tcl -- p_visible_tcl
            ) into v_menu_id;
            update im_menus set
                menu_gif_small = p_menu_gif_small
            where menu_id = v_menu_id;
            update im_menus set 
                enabled_p = p_enabled_p
            where menu_id = v_menu_id;
            perform acs_permission__grant_permission(v_menu_id, 459, 'read');
            IF p_visible_profile is not null THEN
                perform acs_permission__grant_permission(v_menu_id, p_visible_profile, 'read');
            END IF;
    END IF;
    return v_menu_id;
end;$body$ language 'plpgsql';

create or replace function create_or_update_menu (integer, varchar, varchar, varchar, varchar, integer, varchar, varchar, varchar, varchar)
    returns integer as $body$

    DECLARE

        p_menu_id           alias for $1;
        p_package_name          alias for $2;
        p_label            alias for $3;
        p_name           alias for $4;
        p_url           alias for $5;
        p_sort_order            alias for $6;
        p_parent_menu_label         alias for $7;
        p_visible_tcl          alias for $8;
        p_menu_gif_small alias for $9;
        p_enabled_p alias for $10;

        v_count  integer;
        v_result integer;

    BEGIN
    select count(*) into v_count from im_menus
        where menu_id = p_menu_id or label = p_label;
    IF
    v_count = 0
    THEN
        select im_menu__new (
                p_menu_id,              -- p_menu_id
                'acs_object',  -- object_type
                now(),            -- creation_date
                null,              -- creation_user
                null,              -- creation_ip
                null,              -- context_id
                p_package_name,     -- package_name
                p_label, -- label
                p_name,               -- name
                p_url, -- url
                p_sort_order,                  -- sort_order
                (select menu_id from im_menus where name = p_parent_menu_label),  -- parent_menu_id
                p_visible_tcl -- p_visible_tcl
            ) into v_result;
            update im_menus set
                menu_gif_small = p_menu_gif_small
            where menu_id = p_menu_id;
            update im_menus set 
                enabled_p = p_enabled_p
            where menu_id = p_menu_id;

    ELSE
        update im_menus set
            package_name = p_package_name,
            label = p_label,
            name = p_name,
            url = p_url,
            parent_menu_id = (select menu_id from im_menus where name = p_parent_menu_label),
            sort_order = p_sort_order,
            visible_tcl = p_visible_tcl,
            menu_gif_small = p_menu_gif_small,
            enabled_p = p_enabled_p
        where menu_id = p_menu_id or label = p_label;
    END IF;
    select menu_id into p_menu_id from im_menus where label = p_label;
    perform acs_permission__grant_permission(p_menu_id, 459, 'read');
    return p_menu_id;
end;$body$ language 'plpgsql';

create or replace function im_menu__delete_by_label (varchar) 
returns integer as $body$
DECLARE
    p_menu_label  alias for $1;
    p_menu_id integer;
BEGIN
    -- First find out the ID
    select menu_id from im_menus where label = p_menu_label into p_menu_id;
    
    -- Erase the im_menus item associated with the id
    delete from     im_menus
    where       menu_id = p_menu_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where       object_id = p_menu_id;
    
    PERFORM acs_object__delete(p_menu_id);
    return 0;
end;$body$ language 'plpgsql';

create or replace function create_menu (varchar, varchar, varchar, varchar, integer, varchar, varchar, varchar, varchar, integer)
    returns integer as $body$

    DECLARE

        p_package_name          alias for $1;
        p_label            alias for $2;
        p_name           alias for $3;
        p_url           alias for $4;
        p_sort_order            alias for $5;
        p_parent_menu_label         alias for $6;
        p_visible_tcl          alias for $7;
        p_menu_gif_small alias for $8;
        p_enabled_p alias for $9;
        p_visible_profile alias for $10;

        v_menu_id integer;
        v_count  integer;
        v_result integer;

    BEGIN
    select menu_id into v_menu_id from im_menus
        where label = p_label;
    IF
    v_menu_id is null
    THEN
        select im_menu__new (
                null,              -- p_menu_id
                'acs_object',  -- object_type
                now(),            -- creation_date
                null,              -- creation_user
                null,              -- creation_ip
                null,              -- context_id
                p_package_name,     -- package_name
                p_label, -- label
                p_name,               -- name
                p_url, -- url
                p_sort_order,                  -- sort_order
                (select menu_id from im_menus where label = p_parent_menu_label),  -- parent_menu_id
                p_visible_tcl -- p_visible_tcl
            ) into v_menu_id;
            update im_menus set
                menu_gif_small = p_menu_gif_small
            where menu_id = v_menu_id;
            update im_menus set 
                enabled_p = p_enabled_p
            where menu_id = v_menu_id;
            perform acs_permission__grant_permission(v_menu_id, 459, 'read');
            IF p_visible_profile is not null THEN
                perform acs_permission__grant_permission(v_menu_id, p_visible_profile, 'read');
            END IF;
    END IF;
    return v_menu_id;
end;$body$ language 'plpgsql';

SELECT im_menu__new (
                null, 'im_menu', now(), null, null, null,
                'cognovis-core',                        -- package_name
                'categories',                              -- label
                'Categories',                              -- name
                '/cognovis/categories',
                10,                                    -- sort_order
                (select menu_id from im_menus where label = 'master_data'),             -- parent_menu_id
                null
);
SELECT acs_permission__grant_permission(
        (select menu_id from im_menus where label = 'categories'),
        (select group_id from groups where group_name = 'P/O Admins'),
        'read'
);