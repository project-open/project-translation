# packages/intranet-collmex/tcl/intranet-collmex-api-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

ad_library {
    
    Procedures to generate or parse Satzarten from collmex
    
    @author <yourname> (<your email>)
    @creation-date 2012-01-04
    @cvs-id $Id$
}

namespace eval intranet_collmex {

    ad_proc -public CMXKND {
        -customer_id
        {-customer_contact_id ""}
    } {
        Generates a customer record for sending to collmex

        @param customer_id Company we want to send over to collmex
        @param customer_contact_id Company contact we want to send. Defaults to accounting_contact_id, then primary_contact_id. Null otherwise

        use field description from http://www.collmex.de/cgi-bin/cgi.exe?1005,1,help,daten_importieren_kunde
    } {
        if {$customer_contact_id eq ""} {
            set customer_contact_id [db_string accounting "select coalesce(accounting_contact_id,primary_contact_id) from im_companies where company_id = :customer_id" -default ""]
        }

        if {$customer_contact_id eq ""} {
            return ""
        } else {
            db_1row contact_info {
                select * from cc_users where user_id = :customer_contact_id
            }
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"

        db_1row customer_info {
            select *
                from im_offices o, im_companies c
            where c.main_office_id = o.office_id
            and c.company_id = :customer_id
        }

        if {$address_city eq ""} {
            return ""
        }

        # Translation of the country code
        switch $address_country_code {
            "uk" {set address_country_code gb}
            ""   {set address_country_code de} ; # default country code germany
        }
        
        CkCsv_SetCell $csv 0 0 "CMXKND"    

        if {[exists_and_not_null collmex_id]} {
            CkCsv_SetCell $csv 0 1 $collmex_id    
        }

        set locale [lang::user::site_wide_locale -user_id $customer_contact_id]
        CkCsv_SetCell $csv 0 2 1 ; # Firma Nr (internal)
        CkCsv_SetCell $csv 0 3 [string range [im_category_from_id -current_user_id $customer_contact_id -locale $locale $salutation_id] 0 9] ; # Anrede
        CkCsv_SetCell $csv 0 4 [string range $position 0 9] ; # Title
        CkCsv_SetCell $csv 0 5 [string range $first_names 0 39] ; # Vorname
        CkCsv_SetCell $csv 0 6 [string range $last_name 0 39];# Name
        CkCsv_SetCell $csv 0 7 [string range $company_name 0 79] ; # Firma
        CkCsv_SetCell $csv 0 8 [string range $job_title 0 39] ; # Abteilung
        
        if {$address_line2 ne ""} {
            append address_line1 "\n $address_line2"
        }

        CkCsv_SetCell $csv 0 9 [string range $address_line1 0 39] ; # Straße
        
        CkCsv_SetCell $csv 0 10 [string range $address_postal_code 0 9] ; # PLZ
        CkCsv_SetCell $csv 0 11 [string range $address_city 0 39] ; # Ort
        CkCsv_SetCell $csv 0 12 [string range $note 0 1023] ; # Bemerkung

        switch $company_status_id {
            49 {
                # Deleted
                set company_status 3
            }
            47 - 48 - 50 {
                # Inactive
                set company_status 1
            }
            default {
                set company_status 0
            }
        }
        CkCsv_SetCell $csv 0 13 $company_status ; # Inaktiv
        CkCsv_SetCell $csv 0 14 [string range $address_country_code 0 1] ; # Land
        CkCsv_SetCell $csv 0 15 [string range $phone 0 39] ; # Telefon
        CkCsv_SetCell $csv 0 16 [string range $fax 0 39] ; # Telefax
        CkCsv_SetCell $csv 0 17 [string range $email 0 49] ; # E-Mail
        CkCsv_SetCell $csv 0 18 [string range $bank_account_nr 0 19] ; # Kontonr
        CkCsv_SetCell $csv 0 19 [string range $bank_routing_nr 0 19] ; # Blz
        CkCsv_SetCell $csv 0 20 [string range $iban 0 39] ; # Iban
        CkCsv_SetCell $csv 0 21 [string range $bic 0 19] ; # Bic
        CkCsv_SetCell $csv 0 22 [string range $bank_name 0 39] ; # Bankname
        CkCsv_SetCell $csv 0 23 [string range $tax_number 0 19] ; # Steuernummer
        CkCsv_SetCell $csv 0 24 [string range $vat_number 0 19] ; # USt.IdNr
        CkCsv_SetCell $csv 0 25 "6" ; # Zahlungsbedingung - als Zahl codiert, beginnend mit 0 für 30T ohne Abzug, wie im Programm unter "Einstellungen" 
        CkCsv_SetCell $csv 0 26 "" ; # Rabattgruppe - interne Nummer der Rabattgruppe, wie sie bei der Pflege der Rabattgruppe in der ersten Spalte angezeigt wird. 
        CkCsv_SetCell $csv 0 27 "" ; # Lieferbedingung - International genormte INCOTERMS 
        CkCsv_SetCell $csv 0 28 "" ; # Lieferbedingung Zusatz - Ort der Lieferbedingung 
        CkCsv_SetCell $csv 0 29 "1" ; # Ausgabemedium - Als Zahl codiertes Ausgabemedium: 0 = Druck, 1 = E-Mail, 2 = Fax, 3 = Brief. Standard ist E-Mail. 
        CkCsv_SetCell $csv 0 30 "" ; # Kontoinhaber - nur erforderlich falls abweichend 
        CkCsv_SetCell $csv 0 31 "" ; # Adressgruppe - interne Nummer der Adressgruppe, wie sie bei der Pflege der Adressgruppen in der ersten Spalte angezeigt wird. Soll der Kunde mehreren Adressgruppen zugeordnet werden, sind diese jeweils durch ein Komma zu trennen (z.B. 1,2,3). Der Wert '0' entfernt den Kunden aus allen Adressgruppen. Wird nichts angegeben, bleibt die Zuordnung des Kunden zu den Gruppen unverändert. 
        CkCsv_SetCell $csv 0 32 "" ; # eBay-Mitgliedsname - Der eBay-Mitgliedsname des Kunden 
        CkCsv_SetCell $csv 0 33 "" ; # Preisgruppe - interne Nummer der Preisgruppe, wie sie bei der Pflege der Preisgruppe in der ersten Spalte angezeigt wird. 
        CkCsv_SetCell $csv 0 34 "EUR" ; # Währung (ISO-Codes)
        CkCsv_SetCell $csv 0 35 "" ; # Vermittler - Mitarbeiter Nummer des Vermittlers für die Provisionsabrechnung. 
        CkCsv_SetCell $csv 0 36 "" ; # Kostenstelle
        CkCsv_SetCell $csv 0 37 "" ; # Wiedervorlage am
        CkCsv_SetCell $csv 0 38 "" ; # Liefersperre - 1 = Liefersperre 
        CkCsv_SetCell $csv 0 39 "" ; # Baudienstleister - 1 = Bau/Reinigungs-Dienstleister 
        CkCsv_SetCell $csv 0 40 "" ; # Lief-Nr. bei Kunde - Die eigene Lieferantennummer beim Kunden 
        switch $locale {
            de_DE {
                set ausgabesprache 0
            }
            default {
                set ausgabesprache 1
            }
        }
        
        CkCsv_SetCell $csv 0 41 $ausgabesprache ; # Ausgabesprache - 0 = Deutsch , 1 = Englisch 
        CkCsv_SetCell $csv 0 42 "" ; # CC - Kopie der E-Mails an diese Adresse senden 
        CkCsv_SetCell $csv 0 43 "" ; # Telefon2
        CkCsv_SetCell $csv 0 44 "" ; # Lastschrift-Mandatsreferenz - Eindeutige Identifikation des Lastschrift-Mandats 
        CkCsv_SetCell $csv 0 45 "" ; # Datum Unterschrift  - Datum der Unterschrift des Lastschrift-Mandats 
        CkCsv_SetCell $csv 0 46 "" ; # 1 = Mahnsperre 
        CkCsv_SetCell $csv 0 47 "" ; # 1 = keine Mailings 
        CkCsv_SetCell $csv 0 48 "" ; # 1 = Privatperson 
        CkCsv_SetCell $csv 0 49 [string range $url 0 79]  ; # URL
        CkCsv_SetCell $csv 0 50 "" ; # 0 = Teil-Lieferungen nicht erlaubt, 1 = Teil-Lieferungen erlaubt. 
        CkCsv_SetCell $csv 0 51 "" ; # 0 = Teil-Rechnungen nicht erlaubt, 1 = Teil-Rechnungen erlaubt. 
   
        return [CkCsv_saveToString $csv]
    }

    ad_proc -public CMXLIF {
        -provider_id
        {-provider_contact_id ""}
    } {
        Generates a provider record for sending to collmex

        @param provider_id Company we want to send over to collmex
        @param provider_contact_id Company contact we want to send. Defaults to accounting_contact_id, then primary_contact_id. Null otherwise

        use field description from http://www.collmex.de/cgi-bin/cgi.exe?1005,1,help,daten_importieren_kunde
    } {
        if {$provider_contact_id eq ""} {
            set provider_contact_id [db_string accounting "select coalesce(accounting_contact_id,primary_contact_id) from im_companies where company_id = :provider_id" -default ""]
        }

        if {$provider_contact_id eq ""} {
            return ""
        } else {
            db_1row contact_info {
                select * from cc_users where user_id = :provider_contact_id
            }
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"

        db_1row customer_info {
            select *
                from im_offices o, im_companies c
            where c.main_office_id = o.office_id
            and c.company_id = :provider_id
        }


        # Translation of the country code
        switch $address_country_code {
            "uk" {set address_country_code gb}
            ""   {set address_country_code de} ; # default country code germany
        }

        if {$address_city eq ""} {
            return ""
        }
        
        CkCsv_SetCell $csv 0 0 "CMXLIF"    
             if {[exists_and_not_null collmex_id]} {
            CkCsv_SetCell $csv 0 1 $collmex_id    
        }

        set locale [lang::user::site_wide_locale -user_id $provider_contact_id]
        CkCsv_SetCell $csv 0 2 1 ; # Firma Nr (internal)
        CkCsv_SetCell $csv 0 3 [string range [im_category_from_id -current_user_id $provider_contact_id -locale $locale $salutation_id] 0 9] ; # Anrede
        CkCsv_SetCell $csv 0 4 [string range $position 0 9] ; # Title
        CkCsv_SetCell $csv 0 5 [string range $first_names 0 39] ; # Vorname
        CkCsv_SetCell $csv 0 6 [string range $last_name 0 39];# Name
        if {[string match "Freelance*" $company_name]} {
            CkCsv_SetCell $csv 0 7 "" ; # Firma - not present, single freelancer without company name
        } else {
            CkCsv_SetCell $csv 0 7 [string range $company_name 0 79] ; # Firma
        }
        CkCsv_SetCell $csv 0 8 [string range $job_title 0 39] ; # Abteilung
        
        if {$address_line2 ne ""} {
            append address_line1 "\n $address_line2"
        }        

        CkCsv_SetCell $csv 0 9 [string range $address_line1 0 39] ; # Straße
        
        CkCsv_SetCell $csv 0 10 [string range $address_postal_code 0 9] ; # PLZ
        CkCsv_SetCell $csv 0 11 [string range $address_city 0 39] ; # Ort
        CkCsv_SetCell $csv 0 12 [string range $note 0 1023] ; # Bemerkung

        switch $company_status_id {
            49 {
                # Deleted
                set company_status 3
            }
            47 - 48 - 50 {
                # Inactive
                set company_status 1
            }
            default {
                set company_status 0
            }
        }
        CkCsv_SetCell $csv 0 13 $company_status ; # Inaktiv
        CkCsv_SetCell $csv 0 14 [string range $address_country_code 0 1] ; # Land
        CkCsv_SetCell $csv 0 15 [string range $phone 0 39] ; # Telefon
        CkCsv_SetCell $csv 0 16 [string range $fax 0 39] ; # Telefax
        CkCsv_SetCell $csv 0 17 [string range $email 0 49] ; # E-Mail
        CkCsv_SetCell $csv 0 18 [string range $bank_account_nr 0 19] ; # Kontonr
        CkCsv_SetCell $csv 0 19 [string range $bank_routing_nr 0 19] ; # Blz
        CkCsv_SetCell $csv 0 20 [string range $iban 0 39] ; # Iban
        CkCsv_SetCell $csv 0 21 [string range $bic 0 19] ; # Bic
        CkCsv_SetCell $csv 0 22 [string range $bank_name 0 39] ; # Bankname
        CkCsv_SetCell $csv 0 23 [string range $tax_number 0 19] ; # Steuernummer
        CkCsv_SetCell $csv 0 24 [string range $vat_number 0 19] ; # USt.IdNr
        CkCsv_SetCell $csv 0 25 "6" ; # Zahlungsbedingung - als Zahl codiert, beginnend mit 0 für 30T ohne Abzug, wie im Programm unter "Einstellungen" 
        CkCsv_SetCell $csv 0 26 "" ; # Lieferbedingung - International genormte INCOTERMS 
        CkCsv_SetCell $csv 0 27 "" ; # Lieferbedingung Zusatz - Ort der Lieferbedingung 
        CkCsv_SetCell $csv 0 28 "1" ; # Ausgabemedium - Als Zahl codiertes Ausgabemedium: 0 = Druck, 1 = E-Mail, 2 = Fax, 3 = Brief. Standard ist E-Mail. 
        CkCsv_SetCell $csv 0 29 "" ; # Kontoinhaber - nur erforderlich falls abweichend 
        CkCsv_SetCell $csv 0 30 "" ; # Adressgruppe - interne Nummer der Adressgruppe, wie sie bei der Pflege der Adressgruppen in der ersten Spalte angezeigt wird. Soll der Kunde mehreren Adressgruppen zugeordnet werden, sind diese jeweils durch ein Komma zu trennen (z.B. 1,2,3). Der Wert '0' entfernt den Kunden aus allen Adressgruppen. Wird nichts angegeben, bleibt die Zuordnung des Kunden zu den Gruppen unverändert. 
        CkCsv_SetCell $csv 0 31 "" ; # Kundennummer beim Lieferanten
        CkCsv_SetCell $csv 0 32 "EUR" ; # Währung (ISO-Codes)
        CkCsv_SetCell $csv 0 33 "" ; # Telefon2
        switch $locale {
            de_DE {
                set ausgabesprache 0
            }
            default {
                set ausgabesprache 1
            }
        }
        
        CkCsv_SetCell $csv 0 34 $ausgabesprache ; # Ausgabesprache - 0 = Deutsch , 1 = Englisch 
        CkCsv_SetCell $csv 0 35 "" ; # Aufwandskonto
        CkCsv_SetCell $csv 0 36 "" ; # 0 = voller USt, 1 = ermäßigte USt, 2 = keine USt. 
        CkCsv_SetCell $csv 0 37 "" ; # Buchungstext
        CkCsv_SetCell $csv 0 38 "" ; # Kostenstelle

        return [CkCsv_saveToString $csv]
    }

    ad_proc -public CMXLRN {
        -invoice_id:required
        -storno:boolean
    } {
        Import a Lieferantenrechnung into Collmex

        @param invoice_id Invoice to Import
        @return CSV String
    } {
        # Get all the invoice information
        db_1row invoice_data {
            select collmex_id,to_char(effective_date,'YYYYMMDD') as invoice_date, invoice_nr,
            round(vat,0) as vat, round(amount,2) as netto, c.company_id, address_country_code, 
            ca.aux_int2 as konto, cc.cost_center_code as kostenstelle,
            cb.aux_int2 as collmex_payment_term_id, currency, ci.vat_type_id
            from im_invoices i, im_costs ci, im_companies c, im_offices o, im_categories ca, im_cost_centers cc, im_categories cb
            where c.company_id = ci.provider_id
            and c.main_office_id = o.office_id
            and ci.cost_id = i.invoice_id
            and ca.category_id = ci.vat_type_id
                and cb.category_id = ci.payment_term_id
                and cc.cost_center_id = ci.cost_center_id
            and i.invoice_id = :invoice_id
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"

        regsub -all {\.} $netto {,} netto
        CkCsv_SetCell $csv 0 0 "CMXLRN"    

        if {$collmex_id eq ""} {
            set collmex_id [intranet_collmex::update_company -company_id $company_id]
        }

        CkCsv_SetCell $csv 0 1 "$collmex_id" ; # Lieferantennummer
        CkCsv_SetCell $csv 0 2 "1" ; # Firma Nr
        CkCsv_SetCell $csv 0 3 "$invoice_date" ; # Rechnungsdatum
        CkCsv_SetCell $csv 0 4 "$invoice_nr" ; # Rechnungsnummer

        if {$konto eq ""} {
            #set konto [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "KontoBill"]
        }

        # Find if the provide is from germany and has vat.
        if {$vat eq 19 || $vat eq 16} {
            CkCsv_SetCell $csv 0 5 $netto ; # Nettobetrag voller Umsatzsteuersatz
        } else {
            CkCsv_SetCell $csv 0 5 ""
        }

        CkCsv_SetCell $csv 0 6 "" ; # Steuer zum vollen Umsatzsteuersatz
        CkCsv_SetCell $csv 0 7 "" ; # Nettobetrag halber Umsatzsteuersatz
        CkCsv_SetCell $csv 0 8 "" ; # Steuer zum halben Umsatzsteuersatz
        if {$vat eq 19 || $vat eq 16} {
            CkCsv_SetCell $csv 0 9 ""
            CkCsv_SetCell $csv 0 10 ""
        } else {
            CkCsv_SetCell $csv 0 9 $konto ; # Sonstige Umsätze: Konto Nr.
            CkCsv_SetCell $csv 0 10 $netto ; # Sonstige Umsätze: Betrag
        }
        if {$currency eq ""} {set currency "EUR"}
        
        CkCsv_SetCell $csv 0 11 $currency; # Währung (ISO-Codes)
        CkCsv_SetCell $csv 0 12 "" ; # Gegenkonto (1600 per default)
        CkCsv_SetCell $csv 0 13 "" ; # Gutschrift
        CkCsv_SetCell $csv 0 14 "" ; # Belegtext
        CkCsv_SetCell $csv 0 15 "$collmex_payment_term_id" ; # Zahlungsbedingung - Optional. Als Zahl codiert, beginnend mit 0 für 30T ohne Abzug, wie im Programm unter "Einstellungen". Wenn nicht angegeben, wird die Zahlungsbedingung aus dem Kundenstamm ermittelt. 
        if {$vat eq 19 || $vat eq 16} {
            CkCsv_SetCell $csv 0 16 "$konto" ; # KontoNr voller Umsatzsteuersatz - Optional. 3200 (Wareneingang) falls nicht angegeben. 
        } else {
            CkCsv_SetCell $csv 0 16 ""
        }
        CkCsv_SetCell $csv 0 17 "" ; # KontoNr halber Umsatzsteuersatz - Optional. 3200 (Wareneingang) falls nicht angegeben. 
        if {$storno_p} {
            CkCsv_SetCell $csv 0 18 "1" ; # Storno - Umsatz = leer; stornierter Umsatz = 1
        } else {
            CkCsv_SetCell $csv 0 18 "" ; # Storno
        }
        CkCsv_SetCell $csv 0 19 "$kostenstelle" ; # Kostenstelle
        return [CkCsv_saveToString $csv]
    }
}