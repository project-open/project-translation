ad_library {

    Callbacks for cognovis-core

    @author malte.sussdorff@cognovis.de
}

# Callbacks 
ad_proc -public -callback absence_on_change {
    {-absence_id:required}
    {-absence_type_id:required}
    {-user_id:required}
    {-start_date:required}
    {-end_date:required}
    {-duration_days:required}
    {-transaction_type:required}
} {
    Callback to be executed after an absence has been created
} -

ad_proc -public -callback im_user_absence_new_button_pressed {
    {-button_pressed:required}
} {
    This callback is executed after we checked the pressed buttons but before the normal delete / cancel check is executed. 
    
    This allows you to add additional activities based on the actions defined e.g. in the im_user_absence_new_actions. As it is called before delete / cancel you can
    have more actions defined.

} - 


ad_proc -public -callback im_user_absence_new_actions {
} {
    This callback is executed after we build the actions for the new absence form
    
    This allows you to extend in the uplevel the form with any additional actions you might want to add.

} - 

ad_proc -public -callback im_user_absence_info_actions {
} {
    This callback is executed after we build the actions for the absence info page
    
    This allows you to extend in the uplevel the form with any additional actions you might want to add.

} - 

ad_proc -public -callback im_user_absence_perm_check {
    {-absence_id:required}
} {
    This callback is executed first time we determine that we have an absence_id
    
    This allows you to add additional permission checks, especially against ID guessing.

} - 

ad_proc -public -callback im_trace_column_change {
    {-user_id:required}
    {-object_id:required}
    {-table:required}
    {-table:required}
    {-column_name:required}
    {-pretty_name:required}
    {-old_value:required}
    {-new_value:required}
} {
    Callback to be executed after a change in a record
} -


ad_proc -callback im_trace_table_change {
    {-object_id:required}
    {-table:required}
    {-message:required}
} {
    Callback to be executed after a set of changes in a table record
} -

ad_proc -callback im_invoice_after_create -impl cognovis_update_project {
    -object_id:required
    -status_id:required
    -type_id:required
} {
    Update the project_cost_cache
} {
    set project_id [db_string project "select project_id from im_costs where cost_id = :object_id" -default ""]
    if {$project_id ne ""} {
        im_cost_update_project_cost_cache $project_id
    }
}

ad_proc -callback im_invoice_after_update -impl cognovis_delete {
    -object_id:required
    -status_id:required
    -type_id:required
} {
    Delete linked invoices
} {
    if {$status_id eq [im_cost_status_deleted] || $status_id eq [im_cost_status_rejected]} {
        set rel_ids [db_list rel_ids "select object_id_one from acs_rels where object_id_two = :object_id and rel_type = 'im_invoice_invoice_rel'
            UNION select object_id_two from acs_rels where object_id_one = :object_id and rel_type = 'im_invoice_invoice_rel'"]

        foreach rel_id $rel_ids {
            relation_remove $rel_id
        }
    }
}

ad_proc -public -callback im_category_after_create {
    {-object_id:required}
    {-type ""}
    {-status ""}
    {-category_id ""}
    {-category_type ""}
} {
    This is a callback to map attributes and categories using respectively attribute_id and category_id

    @param category_id ID of the category
    @param category_type Type of the category
} -

ad_proc -public -callback im_category_after_update {
    {-object_id:required}
    {-type ""}
    {-status ""}
    {-category_id ""}
    {-category_type ""}
} {
    This is a callback to map attributes and categories using respectively attribute_id and category_id

    @param category_id ID of the category
    @param category_type Type of the category
} -

ad_proc -public -callback im_projects_index_before_render {
    {-view_name:required}
    {-view_type:required}
    {-sql:required}
    {-table_header ""}
    {-variable_set ""}
} {
    This callback is executed before /projects/index is rendered / the sql command actually executed.

    The callback implementation needs to run ad_script_abort in the uplevel, so you don't execute the SQL statement and try to render the component.

    @param view_name view_name used to render the columns.
    @param view_type The view_type. This can be anything, empty string usually means you want to render the component
    @param sql The SQL string which im_timesheet_task_list_component prepares
    @param table_header Name of the table in the spreadsheet (e.g. in Excel).
    @param variable_set A set of variables to pass through
} -

ad_proc -public -callback im_projects_csv1_before_render {
    {-view_name:required}
    {-view_type:required}
    {-sql:required}
    {-table_header ""}
} {
    This callback is executed before im_projects_csv1 is rendered / the sql command actually executed.

    The callback implementation needs to run ad_script_abort in the uplevel, so you don't execute the SQL statement and try to return a CSV file.

    @param view_name view_name used to render the columns.
    @param view_type The view_type. This can be anything, empty string usually means you want to render the component
    @param sql The SQL string which im_timesheet_task_list_component prepares
    @param table_header Name of the table in the spreadsheet (e.g. in Excel).
} -

ad_proc -public -callback im_projects_index_filter {
    {-form_id:required}
} {
    This callback is executed after we generated the filter ad_form

    This allows you to extend in the uplevel the form with any additional filters you might want to add.

    @param form_id ID of the form to which we want to append filter elements
} -

ad_proc -public -callback im_invoices_index_before_render {
    {-view_name:required}
    {-view_type:required}
    {-sql:required}
    {-table_header ""}
    {-variable_set ""}
} {
    This callback is executed before /projects/index is rendered / the sql command actually executed.

    The callback implementation needs to run ad_script_abort in the uplevel, so you don't execute the SQL statement and try to render the component.

    @param view_name view_name used to render the columns.
    @param view_type The view_type. This can be anything, empty string usually means you want to render the component
    @param sql The SQL string which im_timesheet_task_list_component prepares
    @param table_header Name of the table in the spreadsheet (e.g. in Excel).
    @param variable_set A set of variables to pass through
} -


ad_proc -public -callback intranet_chilkat::send -impl cog_updating_bill_invoice {
    -message_id:required
    -from_party_id:required
    -to_party_ids:required
    -body:required
    -subject_required
    {-cc_addr ""}
    {-file_ids ""}
    {-filesystem_files ""}
    {-object_id ""}
} {

    Update the status of the financial document if the mail went through

    @param mesasage_id     the generated message_id for this mail
    @param from_party_id Party ID of the sender
    @param to_party_ids Party ID(s) of the recipient(s)
    @param body Body of the html mail
    @param subject Subject of the send mail
    @param cc_addr Who was in CC (Emails)
    @param file_ids which file_ids where attached
    @param filesystem_files Which files from the filesystem did we attach
    @param object_id The ID of the object that is responsible for sending the mail in the first place
} {
     # Don't update if we don't have an object_id or attached file
    if {$object_id eq "" || $file_ids eq ""} {
        return
    }

    set cost_type_id [db_string cost_type "select cost_type_id from im_costs where cost_id = :object_id" -default ""]
    switch $cost_type_id {
        3700 - 3702 - 3704 - 3706 - 3725 - 3735 - 3740 - 3741 {
            db_dml update_created "update im_costs set cost_status_id = [im_cost_status_outstanding] where cost_status_id = [im_cost_status_created] and cost_id = :object_id"
        }
        default {
            # do nothing
        }
    }
}
