SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.4.8-5.0.2.4.9.sql','');

create or replace function persons_tsearch ()
returns trigger as '
declare
        v_string        varchar;
begin
        select  coalesce(pa.email, '''') || '' '' ||
                coalesce(pa.url, '''') || '' '' ||
                coalesce(pe.first_names, '''') || '' '' ||
                coalesce(pe.last_name, '''') || '' '' ||
                coalesce(u.username, '''') || '' '' ||
                coalesce(u.screen_name, '''') || '' '' ||

                coalesce(home_phone, '''') || '' '' ||
                coalesce(work_phone, '''') || '' '' ||
                coalesce(cell_phone, '''') || '' '' ||
                coalesce(pager, '''') || '' '' ||
                coalesce(fax, '''') || '' '' ||
                coalesce(aim_screen_name, '''') || '' '' ||
                coalesce(msn_screen_name, '''') || '' '' ||
                coalesce(icq_number, '''') || '' '' ||

                coalesce(ha_line1, '''') || '' '' ||
                coalesce(ha_line2, '''') || '' '' ||
                coalesce(ha_city, '''') || '' '' ||
                coalesce(ha_state, '''') || '' '' ||
                coalesce(ha_postal_code, '''') || '' '' ||

                coalesce(wa_line1, '''') || '' '' ||
                coalesce(wa_line2, '''') || '' '' ||
                coalesce(wa_city, '''') || '' '' ||
                coalesce(wa_state, '''') || '' '' ||
                coalesce(wa_postal_code, '''') || '' '' ||

                coalesce(note, '''') || '' '' ||
                coalesce(current_information, '''') || '' '' ||

                coalesce(ha_cc.country_name, '''') || '' '' ||
                coalesce(wa_cc.country_name, '''') || '' '' ||

                coalesce(im_cost_center_name_from_id(e.department_id), '''') || '' '' ||
                coalesce(e.job_title, '''') || '' '' ||
                coalesce(e.job_description, '''') || '' '' ||
                coalesce(e.skills, '''') || '' '' ||
                coalesce(e.educational_history, '''') || '' '' ||
                coalesce(e.last_degree_completed, '''') || '' '' ||
                coalesce(e.termination_reason, '''')

        into    v_string
          from
                parties pa,
                persons pe
                LEFT OUTER JOIN users u ON (pe.person_id = u.user_id)
                LEFT OUTER JOIN users_contact uc ON (pe.person_id = uc.user_id)
                LEFT OUTER JOIN im_employees e ON (pe.person_id = e.employee_id)
                LEFT OUTER JOIN country_codes ha_cc ON (uc.ha_country_code = ha_cc.iso)
                LEFT OUTER JOIN country_codes wa_cc ON (uc.wa_country_code = wa_cc.iso)
        where
                pe.person_id  = new.person_id
                and pe.person_id = pa.party_id
        ;

        perform im_search_update(new.person_id, ''user'', new.person_id, v_string);
        return new;
end;' language 'plpgsql';

update im_component_plugins set package_name = 'cognovis-core', component_tcl = 'cog::freelance::skill_component -user_id $user_id -current_user_id $current_user_id -return_url $return_url' where package_name = 'intranet-freelance' and plugin_name = 'Users Skills Component';
update im_component_plugins set package_name = 'cognovis-core', component_tcl = 'cog::hr::employee_info_component -employee_id $user_id -current_user_id $current_user_id -return_url $return_url -view_name [im_opt_val employee_view_name]' where package_name = 'intranet-hr' and plugin_name = 'User Employee Component';
update im_component_plugins set package_name = 'cognovis-core', component_tcl = 'cog::project::active_projects_component -user_id $user_id -current_user_id $current_user_id' where package_name = 'intranet-core' and plugin_name = 'User Project Portlet';