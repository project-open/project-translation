--
--
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2020-11-09
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.4.4-5.0.2.4.5.sql','');

create or replace function create_menu (varchar, varchar, varchar, varchar, integer, varchar, varchar, varchar, varchar, integer)
    returns integer as $body$

    DECLARE

        p_package_name          alias for $1;
        p_label            alias for $2;
        p_name           alias for $3;
        p_url           alias for $4;
        p_sort_order            alias for $5;
        p_parent_menu_label         alias for $6;
        p_visible_tcl          alias for $7;
        p_menu_gif_small alias for $8;
        p_enabled_p alias for $9;
        p_visible_profile alias for $10;

        v_menu_id integer;
        v_count  integer;
        v_result integer;

    BEGIN
    select menu_id into v_menu_id from im_menus
        where label = p_label;
    IF
    v_menu_id is null
    THEN
        select im_menu__new (
                null,              -- p_menu_id
                'acs_object',  -- object_type
                now(),            -- creation_date
                null,              -- creation_user
                null,              -- creation_ip
                null,              -- context_id
                p_package_name,     -- package_name
                p_label, -- label
                p_name,               -- name
                p_url, -- url
                p_sort_order,                  -- sort_order
                (select menu_id from im_menus where label = p_parent_menu_label),  -- parent_menu_id
                p_visible_tcl -- p_visible_tcl
            ) into v_menu_id;
            update im_menus set
                menu_gif_small = p_menu_gif_small
            where menu_id = v_menu_id;
            update im_menus set 
                enabled_p = p_enabled_p
            where menu_id = v_menu_id;
            perform acs_permission__grant_permission(v_menu_id, 459, 'read');
            IF p_visible_profile is not null THEN
                perform acs_permission__grant_permission(v_menu_id, p_visible_profile, 'read');
            END IF;
    END IF;
    return v_menu_id;
end;$body$ language 'plpgsql';