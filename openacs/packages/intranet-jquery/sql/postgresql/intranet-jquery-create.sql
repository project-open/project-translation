select im_dynfield_widget__new (
        null,                   -- widget_id
        'im_dynfield_widget',   -- object_type
        now(),                  -- creation_date
        null,                   -- creation_user
        null,                   -- creation_ip
        null,                   -- context_id
        'jq_timestamp',            -- widget_name
        'JQ Timestamp',            -- pretty_name
        'JQ Timestamp',            -- pretty_plural
        10007,                  -- storage_type_id
        'text',                 -- acs_datatype
        'jq_datetimepicker',                 -- widget
        'timestamptz',          -- sql_datatype
        '{format "YYYY-MM-DD HH24:MI"}'
);


update im_dynfield_attributes set widget_name = 'jq_timestamp' where widget_name = 'timestamp';