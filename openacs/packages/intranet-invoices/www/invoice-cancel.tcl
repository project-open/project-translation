ad_page_contract {
    Purpose: Cancel an invoice

    @param return_url the url to return to
    @param invoice_id invoice to cancel
    @author malte.sussdorff@cognovis.de
} {
    invoice_id
    target_cost_type_id
} 

# Create the cancellation invoice
im_invoice_copy_new -source_invoice_ids $invoice_id -target_cost_type_id $target_cost_type_id

# Update status to paid
db_dml update_costs "
			update im_costs
			set cost_status_id = [im_cost_status_cancelled]
			where cost_id = $invoice_id"

ad_returnredirect "/intranet-invoices/view?invoice_id=$invoice_id"


