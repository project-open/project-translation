ad_library {

    Callbacks for cognovis-rest module
    
    @author malte.sussdorff@cognovis.de

}

ad_proc -public -callback cog_rest::openapi::response_schema {
    -proc_name
    -object_name
} {
    Callback to allow injecting custom response values

    Typically used to append to the response_object_schema using cog_rest::openapi::response::object_schema 

    @param proc_name Name of the procedure
    @param object_name Name of the object of the procedure
} - 

ad_proc -public -callback cog_rest::object_return_element {
    -proc_name
    -object_class
} {
    Callback to allow injecting custom response values in cog_rest::json_object

    Typically used to append to the return_elements definition or to lappend to the value_json_strings
    
    @param proc_name Name of the procedure
    @param object_class Name of the object of the procedure

    @see cog_rest::json_object
} - 


ad_proc -public -callback cog_rest::cr_file_after_upload {
    -file_item_id
    -file_revision_id
    -context_id
    -context_type
    -user_id
} {
    Callback after a file was uploaded to the content repository

    @param file_item_id ItemId of the file we upload
    @param file_revision_id Revision of the file we uploaded
    @param context_id Context which was Provided
    @param context_type Object Type of the context
    @param user_id User who uploaded the file
} -

namespace eval cog_rest::invoice {}

ad_proc -public -callback cog_rest::invoice::linked_objects {
    -invoice_id
    -cost_type_id
} {
    Callback to allow injecting additional linked objects based of invoice type
} - 