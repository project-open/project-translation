ad_page_contract {
    Purpose: Loads Open Items

    @author malte.sussdorff@cognovis.de
} {
    {company_id ""}
}

set user_id [ad_maybe_redirect_for_registration]
if {![im_permission $user_id add_payments]} {
    ad_return_complaint 1 "<li>[_ intranet-payments.lt_You_have_insufficient]"
    return
}

set open_items_html [intranet_collmex::open_items_get -company_id $company_id]
