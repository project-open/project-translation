ad_library {
    REST Procedures for Timesheet and Timesheets tasks
    @author malte.sussdorff@cognovis.de
}


namespace eval cog_rest::json_object {
    ad_proc timesheet_task {} {
        @return task object im_timesheet_task Timesheet task
        @return task_assignee json_object user Information about the person who added the note
        @return task_status category "Intranet Project Status" Status_id of the task
        @return percent_completed number How much of the task is already finished. Does not have to relate to hours
        @return planned_units number How many units are planned for this task
        @return billable_units number How many units can you charge the customer for
        @return logged_hours number How many hours have been logged on the task
        @return start_date date-time When is the task starting
        @return end_date date-time Date for the deadline
        @return project object im_project Project in which the task is located

    } - 

    ad_proc timesheet_task_body {} {
        @param task_name string Name of the task
        @param task_assignee_id object user::read Information about the person who added the note
        @param percent_completed number How much of the task is already finished. Does not have to relate to hours
        @param planned_units number How many units are planned for this task
        @param billable_units number How many units can you charge the customer for
        @param start_date date-time When is the task starting
        @param end_date date-time Date for the deadline
        @param material_id integer Material we want to create the task for / with
        @param uom_id category "Intranet UoM" What unit of measure do we have for the timesheet task.
    } - 

    ad_proc timesheet_entry {} {
        @return hour_id integer Identifier for the timesheet entry
        @return task named_id Task ID and name
        @return project object im_project Project in which the task is located
        @return user object person::read user Who has logged the time on this entry
        @return day date For which day was the entry
        @return creation_date date-time When was the entry added
        @return note string What was done during that time - as shown to the customer
        @return internal_note string What was done during that time - Internal comment on the time logging, not shown to the customer
        @return hours number how many hours were logged on that entry
    } - 

    ad_proc timesheet_entry_body {} {
        @param user_id object im_person::read Who has logged the time on this entry
        @param day date For which day was the entry
        @param note string What was done during that time - Shown to the customer
        @param internal_note string What was done during that time - Internal comment on the time logging, not shown to the customer
        @param hours number how many hours were logged on that entry
    } - 
}

ad_proc -public cog_rest::get::timesheet_task {
    { -task_id "" }
    { -project_id "" }
    { -task_assignee_id ""}
    -rest_user_id:required
} {
    Information about tasks in the system. Typically for those in a project

    @param task_id object im_timesheet_task::read Task we would like to get information for
    @param project_id object im_project::read Project we want to get the tasks for
    @param task_assignee_id object person::read User we want to get the timesheets for

    @return timesheet_tasks json_array timesheet_task Array of Tasks (Timesheet)
} {

    set timesheet_tasks [list]
    set where_clause_list [list]

    if {$task_id ne ""} {
        lappend where_clause_list "t.task_id = :task_id"
    }

    if {$project_id ne ""} {
        lappend where_clause_list "p.parent_id = :project_id"
    }

    if {$task_assignee_id ne ""} {
        lappend where_clause_list "t.task_assigneed_id = :task_assignee_id"
    } else {
        # Check if we have permission to see all timesheet tasks
        set view_all_tasks_p 1
        if {!$view_all_tasks_p} {
            lappend where_clause_list "acs_permission__permission_p(t.task_id,:rest_user_id,'read') = 't'"
        }
    }

    if {[llength $where_clause_list] eq 0} {
        lappend where_clause_list "t.task_assignee_id = :rest_user_id"
    }

    lappend where_clause_list "t.task_id = p.project_id"

    db_foreach tasks "
        select
            t.task_id, t.task_type_id, t.task_status_id, t.uom_id, t.planned_units, t.billable_units,
            t.task_assignee_id, p.parent_id as project_id, p.project_status_id as task_status_id,
            to_char(p.start_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as start_date, 
            to_char(p.end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') end_date,
            p.reported_hours_cache as logged_hours
        FROM
            im_projects p,
            im_timesheet_tasks t
        WHERE
            [join $where_clause_list " and "]        
    " {

        if {$planned_units >0} {
            if {$logged_hours >0} {
                set percent_completed [expr $logged_hours / $planned_units *100 ]
            } else {
                set percent_completed 0
            }
        } else {
            set percent_completed 100
        }
        if {$task_assignee_id ne ""} {
            set task_assignee [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $task_assignee_id -rest_user_id $rest_user_id]]
        } else {
            set task_assignee ""
        }

        lappend timesheet_tasks [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::timesheet_task {
    -project_id:required
    -rest_user_id:required
    -task_name:required
    { -task_assignee_id ""}
    { -planned_units ""}
    { -billable_units: ""}
    {-start_date ""}
    {-end_date ""}
    {-material_id ""}
    {-uom_id ""}
} {
    Create a new timesheet task

    @param project_id object im_project::read Project in which we want to create the timesheet task
    @param timesheet_task_body request_body Task information

    @return timesheet_task json_object timesheet_task Newly created task
} {

    # Sensible defaults - Might want to change in the future
    if {$material_id eq ""} {
        set material_id [db_string default_cost_center "
            select material_id from im_timesheet_tasks group by material_id order by count(*) DESC limit 1
        " -default ""]
    }

    if {$uom_id eq ""} {
        set uom_id [db_string uom "select material_uom_id from im_materials where material_id = :material_id"]
    }

    regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $task_name]] "_" task_nr

    set task_type_id 9500
    set task_status_id 76
    if {$start_date eq ""} {
        set start_date "now()"
    }

    set task_id [db_string new_task "
        SELECT im_timesheet_task__new (
                null,               -- p_task_id
                'im_timesheet_task',    -- object_type
                now(),                  -- creation_date
                :rest_user_id,                   -- creation_user
                null,                   -- creation_ip
                :project_id,                   -- context_id
                :task_nr,
                :task_name,
                :project_id,
                :material_id,
                null,
                :uom_id,
                :task_type_id,
                :task_status_id,
                null
        );" -default ""]
    
    if {$task_id ne ""} {
        db_dml update_task "update im_timesheet_tasks set planned_units = :planned_units, billable_units = :billable_units, task_assignee_id = :task_assignee_id where task_id = :task_id"
        db_dml update_task_project "update im_projects set start_date = :start_date, end_date = :end_date where project_id = :task_id"
        set timesheet_task [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::timesheet_task -task_id $task_id -rest_user_id $rest_user_id]]
    } else {
        set timesheet_task ""
    }
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::put::timesheet_task {
    -task_id:required
    -rest_user_id:required
    -task_name:required
    { -task_assignee_id "" }
    { -percent_completed "" }
    { -planned_units ""}
    { -billable_units: "" }
    { -start_date ""}
    { -end_date "" }
    {-material_id ""}
    {-uom_id ""}
} {
    Update a timesheet task

    @param task_id object im_timesheet_task::read Task which we want to update
    @param timesheet_task_body request_body Task information

    @return timesheet_task json_object timesheet_task Newly created task
} {
    set task_update_sql_list [list]

    foreach var [list material_id uom_id planned_units billable_units task_assignee_id] {
        if {[set $var] ne ""} {
            lappend task_update_sql_list "$var = :$var"
        }
    }

    if {[llength $task_update_sql_list]>0} {
        db_dml update_timesheet_task "
            UPDATE im_timesheet_tasks SET
                [join $task_update_sql_list " , "]
            WHERE
                task_id = :task_id
            "
    }

    set project_name $task_name
    set project_update_sql_list [list]
    foreach var [list project_name start_date end_date percent_completed] {
        if {[set $var] ne "" && [set $var] ne "null"} {
            lappend project_update_sql_list "$var = :$var"
        }
    }

    if {[llength $project_update_sql_list]>0} {
        db_dml update_timesheet_project "
            UPDATE im_projects SET
                [join $project_update_sql_list " , "]
            WHERE
                project_id = :task_id
            "
    }

    set timesheet_task [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::timesheet_task -task_id $task_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::timesheet_task {
    -task_id:required
    { -migrate_hours_p "0" }
    -rest_user_id:required
} {
    Delete a timesheet task if it has no logged hours or we migrate them to the (parent) project

    @param task_id object im_project::read Project in which we want to create the timesheet task
    @param migrate_hours_p boolean Should we migrate any hours to the project?

    @return errors json_array error Array of errors found
} {
    set errors [list]

    set logged_hour_ids [db_list hours "select hour_id from im_hours where project_id = :task_id"]
    if {[llength $logged_hour_ids] > 0} {
        if {$migrate_hours_p} {
            set project_id [db_string project "select parent_id from im_projects where project_id = :task_id"]
            db_dml update_hours "update im_hours set project_id = :project_id where project_id in ([template::util::tcl_to_sql_list $logged_hour_ids])"
        } else {
            set parameter "Hours"
            set object_id $task_id
            set err_msg "Can't delete task as it has logged hours and we should not move them"
            lappend errors [cog_rest::json_object]
        }
    }
    if {[llength $errors] eq 0} {
        set result [cog::project::nuke -current_user_id $rest_user_id $task_id ]
        if {$result ne ""} {
            set err_msg "$result"
            set parameter "[im_name_from_id $task_id] - $task_id"
            lappend errors [cog_rest::json_object]
        }
    }

    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Hours
#---------------------------------------------------------------

ad_proc -public cog_rest::get::timesheet_entry {
    { -project_id "" }
    { -user_id ""}
    { -hour_id ""}
    -rest_user_id:required
} {
    Get the logged hours for a project and or user

    @param project_id integer Project or Task for which we want to get hours.
    @param user_id object user::* User who logged the hours in the system
    @param hour_id integer Timesheet entry (if we want to get a single one)

    @return timesheet_entries json_array timesheet_entry Timesheet entries
} { 
    set timesheet_entries [list]

    set where_clause_list [list]
    
    lappend where_clause_list "p.project_id = h.project_id"
    lappend where_clause_list "h.hours is not null"

    if {$project_id ne ""} {
        lappend where_clause_list "h.project_id in (
                select	children.project_id
                from	im_projects parent,
                    im_projects children
                where
                    children.tree_sortkey between
                        parent.tree_sortkey
                        and tree_right(parent.tree_sortkey)
                    and parent.project_id = :project_id
                    UNION
                    select :project_id as project_id
            )"
    }
    
    if {$user_id eq "" && $project_id eq "" && $hour_id eq ""} {
        set user_id $rest_user_id
    }

    if {$user_id ne ""} {
        set view_hours_all_p [im_permission $rest_user_id "view_hours_all"]
        if {$view_hours_all_p} {
            lappend where_clause_list "h.user_id = :user_id"
        } else {
            lappend where_clause_list "h.user_id = :rest_user_id"
        }
    }

    if {$hour_id ne ""} {
        lappend where_clause_list "h.hour_id = :hour_id"
    }

    set timesheet_entries_sql "
        select 
            h.hour_id, h.hours, h.user_id,
            h.note, h.internal_note,
            to_char(h.day,'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as day,
            to_char(h.creation_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as creation_date,
            p.project_id as task_id,
            p.parent_id as project_id
        from
            im_hours h,
            im_projects p
        where
            [join $where_clause_list " and "]
        order by day
    "

    db_foreach timesheet_entries $timesheet_entries_sql {

        lappend timesheet_entries [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::timesheet_entry {
    -project_id:required
    { -user_id "" }
    { -day "" }
    { -note "" }
    { -internal_note ""}
    { -hours "" }
    -rest_user_id:required
} {
    @param project_id integer Project or Task for which we want to enter hours.

    @param timesheet_entry_body request_body Timesheet entry information

    @return timesheet_entry json_object timesheet_entry Timesheet entries
} {
    # Log hours by default for the rest_user_id
    if {$user_id eq ""} {
        set user_id $rest_user_id
    }

    if {$user_id ne $rest_user_id} {
        # only admins can log time for somebody else
        if {![im_permission $rest_user_id "add_hours_all"]} {
            cog_rest::error -http_status 400 -message "At the moment we don't support logging hours on somebody else behalf"
        }
    }

    if {$hours eq ""} {
        cog_rest::error -http_status 400 -message "we need to get information how many hours to log"
    }
    
    set material_id [db_string material "select material_id from im_timesheet_tasks where task_id = :project_id" -default ""]

    if {$day eq ""} {
        set day "now()"
    }
    
    # check that the user can actually log more hours for the day
    set max_hours_per_day [parameter::get_from_package_key -package_key intranet-timesheet2 -parameter TimesheetMaxHoursPerDay -default 999]
    if {$max_hours_per_day < 999} {
        set hours_today [expr [db_string hours "select sum(hours) from im_hours where user_id = :user_id and day::date = ${day}::date" -default 0] + $hours] 
        if {$max_hours_per_day < $hours_today} {
            cog_rest::error -http_status 400 -message "we can't log more than $max_hours_per_day on a single day"
        }
    }

    set hour_id [db_string hour "select  nextval('im_hours_seq'::regclass) from dual"]

    db_dml hours_insert "
        insert into im_hours (
            hour_id,user_id, project_id,
            day, hours, material_id,
            note, internal_note
            ) values (
            :hour_id, :user_id, :project_id,
            '$day', :hours, :material_id,
            :note, :internal_note
            )"
    im_timesheet_update_timesheet_cache -project_id $project_id
    im_timesheet2_sync_timesheet_costs -project_id $project_id

    set timesheet_entry [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::timesheet_entry -hour_id $hour_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::timesheet_entry {
    -hour_id:required
    { -user_id "" }
    { -day "" }
    { -note "" }
    { -internal_note ""}
    { -hours "" }
    -rest_user_id:required
} {
    @param hour_id integer Timesheet Entry we want to update

    @param timesheet_entry_body request_body Timesheet entry information

    @return timesheet_entry json_object timesheet_entry Timesheet entries
} {
    # Check if the hour_id is valid
    set valid_p [db_0or1row hours "select user_id as original_user_id, project_id from im_hours where hour_id = :hour_id"]
    if {$valid_p eq ""} {
        cog_rest::error -http_status 400 -message "You have provided an invalid hour_id"
    }

    if {$user_id ne ""} {
        if {$user_id ne $rest_user_id} {
            # only admins can log time for somebody else
            if {![im_permission $rest_user_id "add_hours_all"]} {
                cog_rest::error -http_status 403 -message "You are not allowed to log hours on somebody else behalf"
            }
        }
    } else {
        set user_id $rest_user_id
    }

    if {$user_id ne $original_user_id} {
        if {![im_permission $rest_user_id "add_hours_all"]} {
            cog_rest::error -http_status 403 -message "You are not allowed to change the hour log to a different user"
        }
    }

    set update_sql_list [list]
    foreach var [list user_id day note hours] {
        if {[set $var] ne ""} {
            lappend update_sql_list "$var = :$var"
        }
    }

    if {[llength $update_sql_list]>0} {
        db_dml update_invoice_item "
            UPDATE im_hours SET
                [join $update_sql_list " , "]
            WHERE
                hour_id = :hour_id
        "
    }

    im_timesheet_update_timesheet_cache -project_id $project_id
    im_timesheet2_sync_timesheet_costs -project_id $project_id

    set timesheet_entry [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::timesheet_entry -hour_id $hour_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::timesheet_entry {
    -hour_id:required
    -rest_user_id:required
} {
    Remove a timesheet entry if possible

    @param hour_id integer Timesheet Entry we want to remove

    @return errors json_array error Array of errors found
} {
    set errors [list]
    set valid_p [db_0or1row hours "select user_id, project_id, invoice_id from im_hours where hour_id = :hour_id"]
    if {$valid_p eq ""} {
        set parameter "Hour"
        set object_id $hour_id
        set err_msg "You have provided an invalid hour_id"
        lappend errors [cog_rest::json_object]
    }
    
    if {$user_id ne $rest_user_id} {
        # Check if the rest user is an admin
        if {![im_permission $rest_user_id "add_hours_all"]} {
            set parameter "User"
            set object_id $user_id
            set err_msg "Not authorized to delete hours for a different user"
            lappend errors [cog_rest::json_object]
        }
    }

    # Check for invoice
    if {$invoice_id ne ""} {
        set parameter "Invoice"
        set object_id $invoice_id
        set err_msg "Can't delete hours which are included in an invoice"
        lappend errors [cog_rest::json_object]
    }

    if {[llength $errors] eq 0} {
        db_dml hours_delete "
                delete  from im_hours
                where   hour_id = :hour_id
        "

        im_timesheet_update_timesheet_cache -project_id $project_id
        im_timesheet2_sync_timesheet_costs -project_id $project_id
    }
    # To be implemented
    return [cog_rest::json_response]
}