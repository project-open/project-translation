namespace eval ::template::widget {;}

ad_proc -public template::widget::jq_multiselect {
    element_reference
    tag_attributes
} {
    Render a select widget using jquery multiselect plugin  to comfortably select multiple target languages without
    having to worry about ctrl- clicking in the entry field and potentially loosing my whole selection if I miss clicked
    once.

    @param element_reference Reference variable to the form element
    @param tag_attributes HTML attributes to add to the tag

    @return Form HTML for widget
} {

    upvar $element_reference element

    if { [info exists element(html)] } {
        array set attributes $element(html)
    }

    array set attributes $tag_attributes

    set attributes(multiple) {}

    # Determine the size automatically for a multiselect
    if { ! [info exists attributes(size)] } {
        
        set size [llength $element(options)]
        if { $size > 8 } {
            set size 8
        }
        set attributes(size) $size
    }

    # Support for multiple elements
    set element(values) [lindex $element(values) 0]
    set output [template::widget::menu \
                $element(name) $element(options) $element(values) attributes $element(mode)]

    global __jq_multiselect
    if { ![info exists __jq_multiselect] } {
        append output {
    <link href="/intranet-jquery/jquery_multiselect/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
    <script src="/intranet-jquery/jquery_multiselect/js/jquery.multi-select.js" type="text/javascript"></script>
        }
        set __jq_multiselect 1
    }

    append output "
<script>
\$(document).ready(function() {
    \$('#$element(name)').multiSelect();
});
</script>
"
    return $output
}

ad_proc -public template::widget::jq_datetimepicker {
    element_reference
    tag_attributes
} {
    @author ND
} {
    upvar $element_reference element

    if { [info exists element(mode)] && $element(mode) eq {display} } {
        return [::template::widget::timestamp element $tag_attributes]
    }

    if { ![info exists element(value)] || [lindex $element(value) 0] eq ""} {
        set element(value) [template::util::date::from_ansi [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"] "YYYY-MM-DD HH24:MI"]
    }

    if {[llength $element(value)]>2} {
        lassign $element(value) year month day hours minutes seconds format
        if {$hours eq ""} {set hours 00}
        if {$minutes eq ""} {set minutes 00}

        set element(value) "${year}-${month}-${day} ${hours}:${minutes}"
    } else {
        set format "YYYY-MM-DD HH24:MI"
    }
    set parsed_dt [clock scan [string range $element(value) 0 15]]
    set element(value) [clock format $parsed_dt -format "%Y-%m-%d %H:%M"]
    set output "<input id=\"$element(name)\" name=\"$element(name)\" type=\"text\" value=\"$element(value)\">"

    global __jq_datetimepicker
    if { ![info exists __jq_datetimepicker] } {
        append output {
    <link href="/intranet-jquery/jquery_datetimepicker/jquery.datetimepicker.css" media="screen" rel="stylesheet" type="text/css">
    <script src="/intranet-jquery/jquery_datetimepicker/jquery.js" type="text/javascript"></script>
    <script src="/intranet-jquery/jquery_datetimepicker/build/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
        }
        set __jq_datetimepicker 1
    }

    append output "
<script>
\$(document).ready(function() {
    \$('#$element(name)').datetimepicker({
      format:'Y-m-d H:i'
    });
});
</script>"

    # Just remember the format for now - in the future, allow
    # the user to enter a freeform format
    append output "<input type=\"hidden\" name=\"$element(name).format\" "
    append output "value=\"$format\" >\n"

    return $output
}
