ad_library {
    cognovis specific procedures for projects
    
    @author malte.sussdorff@cognovis.de
}


ad_proc -public im_project_status_inquiring {

} {
    Return the status for inquiring projects
} {
    return 72
}

ad_proc -public im_project_get_all_members {
    {-project_status_id ""}
    {-group_id "-2"}
} {
    returns a [list] of all the users who are in projects with an OPEN status (or subcategories of open).
} {

    if {"" == $project_status_id} {
	set project_status_id [im_project_status_open]
    }

    set project_list [im_project_options -include_empty 0 -project_status_id $project_status_id -exclude_tasks_p 1 -no_conn_p 1]

    set user_ids [list]
    foreach element $project_list {
	set project_id [lindex $element 1]

	set members [db_list_of_lists select_members {
	    select
	    im_name_from_user_id(u.user_id) as name,
	    u.user_id
	    from
	    users u,
	    acs_rels rels
	    LEFT OUTER JOIN im_biz_object_members bo_rels ON (rels.rel_id = bo_rels.rel_id)
	    LEFT OUTER JOIN im_categories c ON (c.category_id = bo_rels.object_role_id),
	    group_member_map m,
	    membership_rels mr
	    where
	    rels.object_id_one = :project_id
	    and rels.object_id_two = u.user_id
	    and mr.member_state = 'approved'
	    and u.user_id = m.member_id
	    and mr.member_state = 'approved'
	    and m.group_id = :group_id
	    and m.rel_id = mr.rel_id
	    and m.container_id = m.group_id
	    and m.rel_type = 'membership_rel'
	    order by lower(im_name_from_user_id(u.user_id))
	}]

	foreach element $members {
	    set user_id_exists_p 0
	    foreach id $user_ids {
		if {$id eq [lindex $element 1]} {
		    set user_id_exists_p 1
		}
	    }
	    
	    if {$user_id_exists_p eq 0} {
		lappend user_ids [lindex $element 1]
	    }
	}
    }
    
    return $user_ids
}

ad_proc -public im_parent_projects {
    -project_ids
    -start_with_leaf:boolean
} {
    Return the full list of parent_projects for the list of project_ids
    This is useful if you want to build the path for more then one project,
    but you can call it with a single project_id and get the path for this project
    NOTE: This includes the called for project_ids as well !!
} {
    set project_list [list]

    if {$start_with_leaf_p} {
       set order "desc"
    } else {
       set order "asc"
    }
    
    if {[llength $project_ids] <1} {return ""} else {
        db_foreach parent_projects "WITH RECURSIVE breadcrumb(parent_id, project_name, project_id, tree_sortkey) AS (
            SELECT parent_id, project_name, project_id, tree_sortkey from im_projects where project_id in ([template::util::tcl_to_sql_list $project_ids])
          UNION ALL
            SELECT p.parent_id,p.project_name, p.project_id, p.tree_sortkey
            FROM breadcrumb b, im_projects p
            WHERE p.project_id = b.parent_id)
            select distinct project_id, tree_sortkey from breadcrumb order by tree_sortkey $order" {
            	lappend project_list $project_id
        }
        return $project_list
    }
}


ad_proc im_project_clone_trans_tasks {
    parent_project_id
    new_project_id
} {
    Copy translation tasks and assignments
} {
    cog_log Debug "im_project_clone_trans_tasks parent_project_id=$parent_project_id new_project_id=$new_project_id"
    set errors "<li>Starting to clone translation tasks"

    im_exec_dml clone_project_tasks "im_trans_task__project_clone (:parent_project_id, :new_project_id)"

    append errors "<li>Finished to clone translation tasks"
    return $errors
}

ad_proc -public im_project_base_data_dynamic_component {
    {-project_id}
    {-return_url}
} {
    returns basic project info with dynfields only
} { 
  
    set params [list  [list base_url "/cognovis-core/"]  [list project_id $project_id] [list return_url $return_url]]
    
    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/project-base-data-dynamic"]
    return [string trim $result]
}

ad_proc -public im_project_status_inquiring {} {} {return 72}

namespace eval cog::project {

    ad_proc -public active_projects_component {
        {-show_empty_project_list_p 1}
        {-view_name "project_personal_list" }
        {-order_by_clause ""}
        {-project_type_id 0}
        {-project_status_id 0}
        {-user_id ""}
        {-current_user_id ""}
    } {
        Returns a HTML table with the list of projects of the
        current user. Don't do any fancy with sorting and
        pagination, because a single user won't be a member of
        many active projects.

        @param show_empty_project_list_p Should we show an empty project list?
            Setting this parameter to 0 the component will just disappear
            if there are no projects.
    } {

        if { $current_user_id eq {} } {
            set current_user_id [auth::get_user_id]
        }

        if {$user_id eq ""} {
            set user_id $current_user_id
        }
        
        im_user_permissions $current_user_id $user_id view read write admin
        if {!$read} { 
            return "" 
        }


        if {"" == $order_by_clause} {
            set order_by_clause  [parameter::get_from_package_key -package_key "intranet-core" -parameter "HomeProjectListSortClause" -default "project_nr DESC"]
        }

        # ---------------------------------------------------------------
        # Columns to show:

        set view_id [db_string get_view_id "select view_id from im_views where view_name=:view_name"]
        set column_headers [list]
        set column_vars [list]
        set extra_selects [list]
        set extra_froms [list]
        set extra_wheres [list]

        set column_sql "
            select
                    column_name,
                    column_render_tcl,
                    visible_for,
                    extra_where,
                    extra_select,
                    extra_from
            from
                    im_view_columns
            where
                    view_id=:view_id
                    and group_id is null
            order by
                    sort_order
        "
        db_foreach column_list_sql $column_sql {
            if {"" == $visible_for || [eval $visible_for]} {
                lappend column_headers "$column_name"
                lappend column_vars "$column_render_tcl"
            }
            if {"" != $extra_select} { lappend extra_selects $extra_select }
            if {"" != $extra_from} { lappend extra_froms $extra_from }
            if {"" != $extra_where} { lappend extra_wheres $extra_where }
        }

        # ---------------------------------------------------------------
        # Generate SQL Query

        set extra_select [join $extra_selects ",\n\t"]
        if { ![empty_string_p $extra_select] } {
            set extra_select ",\n\t$extra_select"
        }

        set extra_from [join $extra_froms ",\n\t"]
        if { ![empty_string_p $extra_from] } {
            set extra_from ",\n\t$extra_from"
        }

        set extra_where [join $extra_wheres "and\n\t"]
        if { ![empty_string_p $extra_where] } {
            set extra_where "and\n\t$extra_where"
        }


        if {0 == $project_status_id} { set project_status_id [im_project_status_open] }

        # Project Status restriction
        set project_status_restriction ""
        if {0 != $project_status_id} {
            set project_status_restriction "and p.project_status_id in ([join [im_sub_categories $project_status_id] ","])"
        }

        # Project Type restriction
        set project_type_restriction ""
        if {0 != $project_type_id} {
            set project_type_restriction "and p.project_type_id in ([join [im_sub_categories $project_type_id] ","])"
        }

        set perm_sql "
            (select
                    p.*
            from
                    im_projects p,
                    acs_rels r
            where
                    r.object_id_one = p.project_id and
                    r.object_id_two = :user_id and
                    p.parent_id is null and
                    p.project_type_id not in ([im_project_type_task], [im_project_type_ticket], [im_project_type_opportunity]) and
                    p.project_status_id not in ([im_project_status_deleted], [im_project_status_closed])
                    $project_status_restriction
                    $project_type_restriction
            )"

        set personal_project_query "
            SELECT
                    p.*,
                    to_char(p.end_date, 'YYYY-MM-DD HH24:MI') as end_date_formatted,
                    c.company_name,
                    im_name_from_user_id(project_lead_id) as lead_name,
                    im_category_from_id(p.project_type_id) as project_type,
                    im_category_from_id(p.project_status_id) as project_status,
                    to_char(end_date, 'HH24:MI') as end_date_time
                    $extra_select
            FROM
                    $perm_sql p,
                    im_companies c
                    $extra_from
            WHERE
                    p.company_id = c.company_id
                    $project_status_restriction
                    $project_type_restriction
                    $extra_where
            order by $order_by_clause
        "


        # ---------------------------------------------------------------
        # Format the List Table Header

        # Set up colspan to be the number of headers + 1 for the # column
        set colspan [expr [llength $column_headers] + 1]

        set table_header_html "<tr class=\"tableheader\">\n"
        foreach col $column_headers {
            regsub -all { } $col {_} col_txt
            set col_txt [lang::message::lookup "" intranet-core.$col_txt $col]
            append table_header_html "  <th class=rowtitle>$col_txt</th>\n"
        }
        append table_header_html "</tr>\n"


        # ---------------------------------------------------------------
        # Format the Result Data

        set url "index?"
        set table_body_html ""
        set bgcolor(0) " class=roweven "
        set bgcolor(1) " class=rowodd "
        set ctr 0
        db_foreach personal_project_query $personal_project_query {

            set url [im_maybe_prepend_http $url]
            if { [empty_string_p $url] } {
                set url_string "&nbsp;"
            } else {
                set url_string "<a href=\"$url\">$url</a>"
            }

            # Append together a line of data based on the "column_vars" parameter list
            set row_html "<tr$bgcolor([expr $ctr % 2])>\n"
            foreach column_var $column_vars {
                append row_html "\t<td valign=top>"
                set cmd "append row_html $column_var"
                eval "$cmd"
                append row_html "</td>\n"
            }
            append row_html "</tr>\n"
            append table_body_html $row_html

            incr ctr
        }

        # Show a reasonable message when there are no result rows:
        if { [empty_string_p $table_body_html] } {

            # Let the component disappear if there are no projects...
            if {!$show_empty_project_list_p} { return "" }

            set table_body_html "
                <tr><td colspan=\"$colspan\"><ul><li><b>
                [lang::message::lookup "" intranet-core.lt_There_are_currently_n "There are currently no entries matching the selected criteria"]
                </b></ul></td></tr>
            "
        }
        return "
            <table class=\"table_list_page\" width=\"100%\">
            <thead>$table_header_html</thead>
            <tbody>$table_body_html</tbody>
            </table>
        "
    }


    ad_proc nuke {
        {-current_user_id 0}
        project_id
    } {
        Nuke (complete delete from the database) a project.
        Returns an empty string if everything was OK or an error
        string otherwise.
    } {
        set detailed_explanation ""

        # Use a predefined user_id to avoid a call to ad_conn user_id.
        # ad_conn user_id's connection isn't defined during a DELETE REST request.
        if {0 == $current_user_id} { 
            set current_user_id [auth::get_user_id] 
        }

        # Check for permissions
        im_project_permissions $current_user_id $project_id view read write admin
        if {!$admin} { 
            cog_log -object_id $project_id -user_id $current_user_id Error "User #$current_user_id isn't a system administrator"
            return "User #$current_user_id isn't a system administrator" 
        }

        # Write Audit Trail
        im_audit -user_id $current_user_id -object_id $project_id -action before_nuke

        # ---------------------------------------------------------------
        # Delete
        # ---------------------------------------------------------------
        
        # if this fails, it will probably be because the installation has 
        # added tables that reference the users table

        db_transaction {
        
            # SLA Parameters
            if {[im_table_exists im_sla_parameters]} {
                set slas [db_list slas "select param_id from im_sla_parameters where param_sla_id = :project_id"]
                foreach sla_id $slas {
                    db_string del_sla_param "select im_sla_parameter__delete(:sla_id)"
                }
            }
        
            # Service Hours per SLA (SLA is a sub-type of project)
            if {[im_table_exists im_sla_service_hours]} {
                db_dml del_sla_param "delete from im_sla_service_hours where sla_id = :project_id"
            }

            # Helpdesk Tickets
            if {[im_table_exists im_tickets]} {
                db_dml del_tickets "delete from im_tickets where ticket_id = :project_id"
            }

            # Permissions
            db_dml perms "delete from acs_permissions where object_id = :project_id"

            # Payments
            db_dml reset_payments "
                update im_payments 
                set cost_id = null 
                where cost_id in (
                    select cost_id 
                    from im_costs 
                    where project_id = :project_id
                )"
            
            # Costs
            db_dml reset_invoice_items "
                update im_invoice_items 
                set project_id = null 
                where project_id = :project_id"


            set cost_infos [db_list_of_lists costs "
                select cost_id, object_type 
                from im_costs, acs_objects 
                where cost_id = object_id and project_id = :project_id
            "]
            foreach cost_info $cost_infos {
                set cost_id [lindex $cost_info 0]
                set object_type [lindex $cost_info 1]

                # Delete Multiple-Values associated to this cost item ("Canned Notes")
                db_dml del_multi_values "delete from im_dynfield_attr_multi_value where object_id = :cost_id"

                # Delete references from im_hours to im_costs.
                db_dml hours_costs_link "update im_hours set cost_id = null where cost_id = :cost_id"

                # ToDo: Remove this.
                # Instead, the referencing im_expense_bundles (data type doesn't exist yet)
                # should be deleted with the appropriate destructor method
                db_dml expense_cost_link "update im_expenses set bundle_id = null where bundle_id = :cost_id"

                if {[im_column_exists im_invoice_items created_from_item_id]} {
                    db_dml created_from_item_id "
                    update im_invoice_items 
                    set created_from_item_id = null 
                    where created_from_item_id in (
                        select	item_id
                        from	im_invoice_items
                        where	invoice_id = :cost_id
                    )
                        "
                }
                im_exec_dml del_cost "${object_type}__delete($cost_id)"
            }
        
            # im_notes
            if {[im_table_exists im_notes]} {
                db_dml notes "
                    delete from im_notes
                    where object_id = :project_id
                "
            }

            # Estimate to complete
            if {[im_table_exists im_estimate_to_completes]} {
                db_dml etc "
                    delete from im_estimate_to_completes
                    where etc_project_id = :project_id
                "
            }

            # Forum
            db_dml forum "
                delete from im_forum_topic_user_map 
                where topic_id in (
                    select topic_id 
                    from im_forum_topics 
                    where object_id = :project_id
                )
            "
            db_dml forum "delete from im_forum_topics where object_id = :project_id"

            # Hours
            db_dml hours "delete from im_hours where project_id = :project_id"
        

            # Workflow
            if {[im_table_exists wf_workflows]} {
                db_dml wf_tokens "
                delete from wf_tokens
                where case_id in (
                    select case_id
                    from wf_cases
                    where object_id = :project_id
                )
                "
                db_dml wf_cases "
                delete from wf_cases
                where object_id = :project_id
                "
            }
            
            # Translation & Workflow
            if {[im_table_exists wf_workflows] && [im_table_exists im_trans_tasks]} {
                db_dml wf_tokens "
                delete from wf_tokens
                where case_id in (
                    select case_id
                    from wf_cases
                    where object_id in (
                        select task_id
                        from im_trans_tasks
                        where project_id = :project_id
                    )
                )
                "
                db_dml wf_cases "
                delete from wf_cases
                where object_id in (
                    select task_id
                    from im_trans_tasks
                    where project_id = :project_id
                )
                "

            }


            # Translation Quality
            if {[im_table_exists im_trans_quality_reports]} {
                db_dml trans_quality "
                    delete from im_trans_quality_entries 
                    where report_id in (
                        select report_id 
                        from im_trans_quality_reports 
                        where task_id in (
                            select task_id 
                            from im_trans_tasks 
                            where project_id = :project_id
                        )
                    )
                "
                db_dml trans_quality "
                    delete from im_trans_quality_reports 
                    where task_id in (
                        select task_id 
                        from im_trans_tasks 
                        where project_id = :project_id
                    )"
            }

        
            # Translation
            if {[im_table_exists im_trans_tasks]} {
                db_dml task_actions "
                    delete from im_task_actions 
                    where task_id in (
                        select task_id 
                        from im_trans_tasks
                        where project_id = :project_id
                )"
                db_dml trans_tasks "
                    delete from im_trans_tasks 
                    where project_id = :project_id
                "

                db_dml project_target_languages "
                    delete from im_target_languages 
                    where project_id = :project_id
                "
            }

            
            # Trans RFCs
            if {[im_table_exists im_trans_rfqs]} {
                db_dml trans_rfq_answers "
                    delete from im_trans_rfq_answers
                where answer_project_id = :project_id
                "
                db_dml trans_rfq_answers "
                    delete from im_trans_rfq_answers
                where answer_rfq_id in (
                    select rfq_id
                    from im_trans_rfqs
                    where rfq_project_id = :project_id
                )
                "
                db_dml trans_rfqs "
                delete from im_trans_rfqs
                where rfq_project_id = :project_id
                "
            }
            
            # Gantt
            if {[im_table_exists im_timesheet_tasks]} {
                
                db_dml task_actions "
                delete from im_hours
                where project_id = :project_id
                "

                db_dml task_actions "
                    delete from im_timesheet_tasks
                    where task_id = :project_id
                "
            }

            # MS-Project Warnings
            if {[im_table_exists im_gantt_ms_project_warning]} {
                db_dml im_gantt_ms_project_warnings "
                    delete from im_gantt_ms_project_warning
                    where project_id = :project_id
                "
            }

            # Rule Engine Logs
            if {[im_table_exists im_rule_logs]} {
                db_dml im_rule_logs "
                    delete from im_rule_logs
                    where rule_log_object_id = :project_id
                "
            }

            # Budget Planning
            if {[im_table_exists im_planning_items]} {
                db_dml im_planning_itemss "
                    delete from im_planning_items
                    where item_object_id = :project_id
                "
            }

            # Helpdesk
            if {[im_table_exists im_tickets]} {
                db_dml tickets "
                    delete from im_tickets
                    where ticket_id = :project_id
                "
            }

            # GanttProject
            if {[im_table_exists im_timesheet_task_dependencies]} {
                db_dml del_dependencies "
                delete from im_timesheet_task_dependencies
                where (task_id_one = :project_id OR task_id_two = :project_id)
                "
            }

            if {[im_table_exists im_gantt_projects]} {
                db_dml del_gantt_projects "
                delete from im_gantt_projects
                where project_id = :project_id
                "
            }


            # Skills
            if {[im_table_exists im_object_freelance_skill_map]} {
                db_dml del_skills "
                    delete from im_object_freelance_skill_map
                    where object_id = :project_id
                "
            }
            
            # RFQs
            if {[im_table_exists im_freelance_rfqs]} {
                db_dml del_rfq_answers "
                    delete from im_freelance_rfq_answers
                    where answer_rfq_id in (
                        select	rfq_id
                        from	im_freelance_rfqs
                        where	rfq_project_id = :project_id
                    )
                "
                db_dml del_rfqs "
                    delete from im_freelance_rfqs
                    where rfq_project_id = :project_id
                "
            }

            # Risks
            if {[im_table_exists im_risks]} {
                db_dml del_risks "delete from im_risks where risk_project_id = :project_id"
            }

            # Old im_projects audit
            if {[im_table_exists im_projects_audit]} {
                db_dml del_im_projects_audit "delete from im_projects_audit where project_id = :project_id"
            }

            # Baselines
            if {[im_table_exists im_baselines]} {
                db_dml del_risks "delete from im_projects_audit where baseline_id in (select baseline_id from im_baselines where baseline_project_id = :project_id)"
                db_dml del_risks "delete from im_baselines where baseline_project_id = :project_id"
            }
            
            # Filestorage
            db_dml filestorage "
                delete from im_fs_folder_status 
                where folder_id in (
                    select folder_id 
                    from im_fs_folders 
                    where object_id = :project_id
                )
            "
            db_dml filestorage "
                delete from im_fs_folder_perms 
                where folder_id in (
                    select folder_id 
                    from im_fs_folders 
                    where object_id = :project_id
                )
            "
            db_dml filestorage_files "
                delete from im_fs_files 
                where folder_id in (
                    select folder_id 
                    from im_fs_folders 
                    where object_id = :project_id
                )
            "
            db_dml filestorage "delete from im_fs_folders where object_id = :project_id"


            # Calendar
                if {[im_table_exists cal_items]} {
                db_dml del_cal_items "
                delete from cal_items 
                where	cal_item_id in (
                        select event_id 
                        from acs_events 
                        where	related_object_type = 'im_project' and 
                            related_object_id not in (select project_id from im_projects)
                    )
                "
                db_dml del_acs_events "
                delete	from acs_events 
                where	related_object_type = 'im_project' and 
                    related_object_id not in (select project_id from im_projects)
                "
            }


            set rels [db_list rels "
                select rel_id 
                from acs_rels 
                where object_id_one = :project_id 
                    or object_id_two = :project_id
            "]

            set im_conf_item_project_rels_exists_p [im_table_exists im_conf_item_project_rels]
            set im_ticket_ticket_rels_exists_p [im_table_exists im_ticket_ticket_rels]

            # TS Configuration Objects
            if {[im_table_exists im_timesheet_conf_objects]} {

                db_dml del_conf_object_dependencies "
                update im_hours
                set conf_object_id = null
                where conf_object_id in (
                    select conf_id
                    from im_timesheet_conf_objects
                    where conf_project_id = :project_id
                )
                "

                db_dml del_dependencies "
                    delete from im_timesheet_conf_objects
                    where conf_project_id = :project_id
                "
            }
        
            # Survey responses
            if {[im_table_exists survsimp_responses]} {
                db_dml del_dependencies "
                    delete from survsimp_responses
                    where related_object_id = :project_id or related_context_id = :project_id
                "
            }

            # Relationships
            foreach rel_id $rels {
                db_dml del_rels "delete from group_element_index where rel_id = :rel_id"
                if {[im_column_exists im_biz_object_members skill_profile_rel_id]} {
                    db_dml del_rels "update im_biz_object_members set skill_profile_rel_id = null where skill_profile_rel_id = :rel_id"
                }
                if {[im_table_exists im_gantt_assignment_timephases]} {
                    db_dml del_rels "delete from im_gantt_assignment_timephases where rel_id = :rel_id"
                }
                if {[im_table_exists im_gantt_assignments]} {
                    db_dml del_rels "delete from im_gantt_assignments where rel_id = :rel_id"
                }
                if {[im_table_exists im_agile_task_rels]} {
                    db_dml del_rels "delete from im_agile_task_rels where rel_id = :rel_id"
                }
                db_dml del_rels "delete from im_biz_object_members where rel_id = :rel_id"
                db_dml del_rels "delete from membership_rels where rel_id = :rel_id"
                if {$im_conf_item_project_rels_exists_p} { db_dml del_rels "delete from im_conf_item_project_rels where rel_id = :rel_id" }
                if {$im_ticket_ticket_rels_exists_p} { db_dml del_rels "delete from im_ticket_ticket_rels where rel_id = :rel_id" }
                if {[im_table_exists im_release_items]} {
                    db_dml del_rels "delete from im_release_items where rel_id = :rel_id"
                }
                db_dml del_rels "delete from acs_rels where rel_id = :rel_id"
                db_dml del_rels "delete from acs_objects where object_id = :rel_id"
            }

            db_dml party_approved_member_map "
                delete from party_approved_member_map 
                where party_id = :project_id"
            db_dml party_approved_member_map "
                delete from party_approved_member_map 
                where member_id = :project_id"


            db_dml acs_objects_context_index "
                update acs_objects set context_id = null
                where context_id = :project_id";
            db_dml acs_objects_context_index2 "
                update acs_objects set context_id = null
                where object_id = :project_id";

            
            db_dml acs_object_context_index "
                delete from acs_object_context_index
                where object_id = :project_id OR ancestor_id = :project_id"

            
            db_dml parent_projects "
                -- project_nr and project_path are unique, 
                -- so the parent_id = null statement below will create
                -- duplicate main projects
                update im_projects set 
                    project_nr = project_nr || random(),
                    project_path = project_path || random()
                where parent_id = :project_id"

            # Relocate sub-tasks to the parent, so that they won't
            # appear as main projects
            set parent_parent_id [db_string parent_id "select parent_id from im_projects where project_id = :project_id" -default ""]
            db_dml parent_projects "
                update im_projects 
                set parent_id = :parent_parent_id
                where parent_id = :project_id"

            if {[im_column_exists im_projects program_id]} {
                db_dml program_id "
                update im_projects 
                set program_id  = null 
                where program_id = :project_id"
            }

            if {[im_table_exists biz_object_groups]} {
                db_dml delete_biz_object_groups "
                delete from biz_object_groups 
                where biz_object_id = :project_id"
            }

            if {[im_table_exists webix_notifications]} {
                db_dml notification "
                delete from webix_notifications 
                where context_id = :project_id
                or project_id = :project_id"
            }

            db_dml delete_projects "
                delete from im_projects 
                where project_id = :project_id"
            db_dml delete_project_biz_objs "
                delete from im_biz_objects
                where object_id = :project_id"
            db_dml delete_project_acs_obj "
                delete from acs_objects
                where object_id = :project_id"

        } on_error {
            if {[ regexp {integrity constraint \([^.]+\.([^)]+)\)} $errmsg match constraint_name]} {
                set sql "select table_name from user_constraints 
                    where constraint_name=:constraint_name"
                db_foreach user_constraints_by_name $sql {
                    append detailed_explanation "<p>[_ intranet-core.lt_It_seems_the_table_we]"
                }
            }
            append detailed_explanation "<pre>$errmsg</pre>"
        }

        if {$detailed_explanation ne ""} {
            cog_log -object_id $project_id -user_id $current_user_id Notice $detailed_explanation
        }
        return $detailed_explanation
    }
}