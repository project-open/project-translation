ad_library {

    Callbacks for FS 

    @creation-date July 08 2010
    @author malte.sussdorff@cognovis.de

}

# Deleting the project folder 
ad_proc -public -callback im_project_after_delete -impl fs_folder {
    {-object_id:required}
    {-status_id}
    {-type_id}
} {
    
    Delete the folders from the project
    
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2010-09-27
    
    @param project_id Project_id of the project
    @return             nothing
    @error
} {
    
    if {[db_0or1row select_folder_id "
	select object_id_two, rel_id
	from acs_rels 
	where object_id_one = :object_id
	and rel_type = 'project_folder'
    "]} {
	
        db_string del_rel "select acs_rel__delete(:rel_id) from dual"
        
        content::folder::delete -folder_id $object_id_two -cascade_p 1
    }
}


ad_proc -public -callback im_project_after_update -impl intranet-fs_update_parent_folder {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Move the folder to the new parent project
} {

    set project_id $object_id
    
    set project_folder_id [db_string project_folder_id {
	    SELECT object_id_two FROM acs_rels WHERE object_id_one = :project_id AND rel_type = 'project_folder'
    } -default ""]
    
    
    set new_parent_id [db_string parent_id {
	    SELECT parent_id FROM im_projects WHERE project_id = :project_id
    } -default ""]
    
    if {[exists_and_not_null new_parent_id]} {
        set new_parent_folder_id [db_string select_folder_id {
            SELECT object_id_two FROM acs_rels WHERE object_id_one = :new_parent_id AND rel_type = 'project_folder'
        } -default ""]
    } else {
        #in case it is a root project the parent folder is the main folder "projects", which is retrieved from intranet-core package instance
        set new_parent_id  [db_string select_package_id {
            SELECT package_id FROM apm_packages WHERE package_key = 'intranet-core'
	    } -default ""]
	
        if {[exists_and_not_null new_parent_id]} {
            set new_parent_folder_id [db_string select_folder_id {
                SELECT object_id_two FROM acs_rels WHERE object_id_one = :new_parent_id AND rel_type = 'package_folder'
            } -default ""]
        }
    }

    # We should disable this for a while
    if {[exists_and_not_null new_parent_folder_id] && 0} {
        # Check this out! API content::item::move later
        db_exec_plsql update_folder_parent_id {
            SELECT content_folder__move(:project_folder_id,:new_parent_folder_id)
        }  
    }
}



ad_proc -public -callback intranet_fs::after_project_folder_create {
	{-project_id:required}
	{-folder_id:required}
} {
	After the project folder has been created, you can do additional things like copying a preset of folders from a template directory to the newly created folder. Very useful for translation folders
	
	@param project_id ID of the project in which the project folder resides
	@param folder_id New folder_id of the created folder for the project
} -

ad_proc -public -callback im_biz_object_member_after_delete -impl intranet_fs_remove_folder_permission {
    {-object_id:required}
    {-object_type:required}
    {-user_id:required}
} {
    Hook for executing callbacks after a user was removed from an object. 
} {
	set folder_id ""
    if {$object_type eq "im_project"} {
		set folder_id [intranet_fs::get_project_folder_id -project_id $object_id]
	}
	if {$object_type eq "im_company"} {
		set folder_id [intranet_fs::get_company_folder_id -company_id $object_id]
	}
	if {$folder_id ne ""} {
	    permission::revoke -party_id $user_id -object_id $folder_id -privilege read
	    permission::revoke -party_id $user_id -object_id $folder_id -privilege create
	    permission::revoke -party_id $user_id -object_id $folder_id -privilege write
	    permission::revoke -party_id $user_id -object_id $folder_id -privilege delete
	    permission::revoke -party_id $user_id -object_id $folder_id -privilege admin
	}
}


ad_proc -public -callback intranet_fs::after_company_folder_create {
	{-company_id:required}
	{-folder_id:required}
} {
	After the company folder has been created, you can do additional things like copying a preset of folders from a template directory to the newly created folder. Very useful for translation folders

	@param project_id ID of the project in which the project folder resides
	@param folder_id New folder_id of the created folder for the project
} -
