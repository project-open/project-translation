ad_page_contract {
    Display  OpenAPI Information of all documented API endpoints
    
    @cvs-id $Id$
} {
    { include_default_p "" }
    { package_key ""}
}

if {$package_key eq ""} {
  set package_key "cognovis-rest"
  if {$include_default_p eq 1} {
    set endpoints [cog_rest::endpoints -include_default]
  } else {
    set endpoints [cog_rest::endpoints]
  }
} else {
    set endpoints [cog_rest::endpoints -package_key $package_key]  
}

#---------------------------------------------------------------
# Handle the paths
#---------------------------------------------------------------
set paths ""

# List of supertypes, used later
set supertypes [db_list supertypes "select distinct supertype from acs_object_types"]

# List of tags used
set global_tags [list]

# List with all component names for parameters
set param_comps [list]
set responses [list]

foreach endpoint $endpoints {

  if {[info exists doc_elements]} {
    array unset doc_elements
  }


  set proc_found_p 0
  foreach operation [list get post] {
    set proc_names($operation) [cog_rest::endpoint_proc -endpoint $endpoint -operation $operation]
    if {$proc_names($operation) ne ""} {
      set proc_found_p 1
    }
  }
  
  # Check if get and post are empty
  if {$proc_found_p} {
    append paths "  /${endpoint}:\n"
  } else {
    continue
  }

  foreach operation [list get post] {

    #---------------------------------------------------------------
    # Get the description of the documentation
    #---------------------------------------------------------------

    set proc_name [cog_rest::endpoint_proc -endpoint $endpoint -operation $operation]
    if {$proc_name ne ""} {
      # Lets find out more
      # Get the documentation into an array first
      array set doc_elements [nsv_get api_proc_doc $proc_name]

      # Get the details from the procedure
      switch $proc_name {
        cog_rest::post::object_type - cog_rest::delete::object_type {
          # No documentation
          continue
        }
        cog_rest::get::object_type {
          # Generic object types
          set description "Using the generic handler for $endpoint . No specific information available.<br />You can use sort, limit, start"
          # Append the parameters

          set summary [db_string object_type_pretty "select pretty_name from acs_object_types where object_type = :endpoint" -default ""]
          set column_names [cog_rest::object_type_columns -include_acs_objects_p 0 \
            -rest_otype $endpoint -data_type_arr data_types -default_arr default_values -required_arr required_arr]

          foreach column_name $column_names {
            set column_description($column_name) "" ; # Eventually get this from the database column or dynfield attributes....
            
            # Manually set format to email
            if {$column_name eq "email"} {
              set data_types($column_name) email
            }

            # Boolean convention
            if {[string match "*_p" $column_name]} {
               set data_types($column_name) boolean
            }
          }

          # Check if the object is a su
          set supertype [db_string supertype "select supertype from acs_object_types where object_type = :endpoint" -default ""]
          switch $supertype {
            acs_object {
              if {[lsearch $supertypes $endpoint]<0} {
                set tags "acs_object"
              } else {
                set tags $endpoint
              }
            }
            default {
              # Check if this is a supertype. then leave it be
              # Ideally this handles im_project which is a supertype, but also a biz_object
              if {[lsearch $supertypes $endpoint]<0} {
                set tags $supertype
              } else {
                set tags $endpoint
              }
            }
          }

          set operationId "${proc_name}::$endpoint"

          set return_description "Generic description"
          set return_title "$endpoint"
          
          set response_comp(${operation}_$endpoint) "      title: '$return_title'\n"
          append response_comp(${operation}_$endpoint) "      type: object\n"
          append response_comp(${operation}_$endpoint) "      properties:\n"
          append response_comp(${operation}_$endpoint) "        placeholder:\n"
          append response_comp(${operation}_$endpoint) "          type: string\n"
          append response_comp(${operation}_$endpoint) "          description: Placeholder description\n"

          lappend responses "${operation}_$endpoint"
        }
        default {
          set file_path_list [split $doc_elements(script) "/"]
          set package_key [lindex $file_path_list 1]

          # Try to set a subtag based on the procs-file
          set procs_file [lindex $file_path_list 3]
          if {![regexp "${package_key}-(.*)-procs.tcl" $procs_file match result]} {
            set tags $package_key
			    } else {
            set tags "${package_key}:$result"            
          }

          set operationId $proc_name

          # Handle the flags to determine boolean / required
          array set flags $doc_elements(flags)
          
          array set default_values $doc_elements(default_values)

          # Get the summary from the summary... Or the object type
          if {[info exists doc_elements(summary)]} {
            set summary [cog_rest::convert_to_yaml_string -string_from "[join $doc_elements(summary) "<br />"]"]
          } else {
            set summary [db_string object_type_pretty "select pretty_name from acs_object_types where object_type = :endpoint" -default ""]
          }
          set description [cog_rest::convert_to_yaml_string -string_from "[lindex $doc_elements(main) 0]"]

          set column_names [list]
          if {[info exists doc_elements(param)]} {
            foreach param $doc_elements(param) {
              regsub -all {"} $param {'} param
              set name [lindex $param 0]
              lappend column_names $name

              set data_type [lindex $param 1]
              switch $data_type {
                integer - string - boolean - number - category - json_array {
                  # The first parameter apparently is the type 
                  set data_types($name) "$data_type"
                  set lrange_start 2
                } 
                default {
                  set lrange_start 1
                  set data_types($name) ""
                }
              }
              
              # Find out if this is a required paramater
              if {[info exists flags($name)]} {
                # Search for required in the flags
                if {[lsearch $flags($name) "required"]<0} {
                  set required_arr($name) 0
                } else {
                  set required_arr($name) 1
                }
              } else {
                set required_arr($name) 0
              }

              set column_description($name) [cog_rest::convert_to_yaml_string -string_from "[lrange $param $lrange_start end]"]
            }
          }

          #---------------------------------------------------------------
          # Return Elements
          #---------------------------------------------------------------

          # Find the object names which we return
          set object_names [list]
          set return_descriptions [list]
          if { [info exists doc_elements(return)] } {
            foreach return_element $doc_elements(return) {
              set return_items [split $return_element]
              set object_name [lindex $return_items 0]
              lappend return_descriptions [lrange $return_items 1 end]
              if { [info exists doc_elements(return_${object_name})]} {
                lappend object_names $object_name
              }
            }
          }

          set return_description "[join $return_descriptions "<br \>"]"
          set return_title "[join $object_names "<br \>"]"

          set response_comp(${operation}_$endpoint) "      title: '$return_title'\n"
          append response_comp(${operation}_$endpoint) "      type: object\n"
          append response_comp(${operation}_$endpoint) "      properties:\n"
          lappend responses "${operation}_$endpoint"          
          
          foreach object_name $object_names {
            set value_json_strings [list]

            #---------------------------------------------------------------
            # Prepare the examples
            #---------------------------------------------------------------
            if {[info exists doc_elements(example_${object_name})]} {
              foreach return_element $doc_elements(example_${object_name}) {  
                # The first item is supposed to be the variable
                set return_items [split $return_element]
                set return_var [lindex $return_items 0]
                set return_example [lrange $return_items 1 end]

                set examples($return_var) $return_example
              }
            }

            #---------------------------------------------------------------
            # The single properties
            #---------------------------------------------------------------
            foreach return_element $doc_elements(return_${object_name}) {  
              # The first item is supposed to be the variable
              set return_items [split $return_element]
              set return_var [lindex $return_items 0]
              set return_type [lindex $return_items 1]

              # Transform the return_type
              switch $return_type {
                category {
                  set return_type "object
          properties:
            id:
              type: integer
              description: 'category_id of the category'
            name:
              type: string
              description: 'Translated category name'\n"
                }
                named_id {
                  set return_type "object
          properties:
            id:
              type: integer
              description: 'id of the named object'
            name:
              type: string
              description: 'Name of the object'\n"
                }
                json_array - date {
                  set return_type "string"
                }
                default {
                  # check if this is a valid type, otherwise set to string
                  if {[lsearch [list array boolean integer number object string] $return_type]<0} {
                    set return_type "string"
                  }
                  
                }
              }

              set return_item_desc [lrange $return_items 2 end]
              append response_comp(${operation}_$endpoint) "        $return_var:\n"
              append response_comp(${operation}_$endpoint) "          type: $return_type\n"
              if {$return_item_desc ne ""} {
                append response_comp(${operation}_$endpoint) "          description: '[cog_rest::convert_to_yaml_string -string_from $return_item_desc]'\n"
              }
              if {[info exists examples($return_var)]} {
                append response_comp(${operation}_$endpoint) "          example: $examples($return_var)\n"
              }
            }
          }

          if {$object_names eq ""} {
            # Ups.. bad documentation?
            append response_comp(${operation}_$endpoint) "        placeholder:\n"
            append response_comp(${operation}_$endpoint) "          type: string\n"
            append response_comp(${operation}_$endpoint) "          description: Placeholder description\n"            
          }
        }
      }

      # Okay... so we have a procedure which can handle this. 
      append paths "    ${operation}:\n"

      # Find the wiki info
      set wiki_url [cog_rest::wiki_url -object_type $endpoint] 
      if {$wiki_url ne ""} {
        append paths "      externalDocs:
        description: project-open Wiki Page
        url: $wiki_url \n"
      } else {
        append paths "      externalDocs:
        description: '$proc_name definition'
        url: [im_system_url][api_proc_url $proc_name] \n"
      }


      # Append summary and description
      if {$summary ne ""} {
        append paths "      summary: $summary\n"
      }

      append paths "      operationId: '$operationId'\n"
      append paths "      description: '$description'\n"
      append paths "      tags:
      - $tags\n"

      # Append the tags so we can add it to the global tags.
      if {[lsearch $global_tags $tags]<0} {
        lappend global_tags $tags
      }

      #---------------------------------------------------------------
      # Paramaters to attach
      #---------------------------------------------------------------
      
      if {[llength $column_names]>0} {   
        append paths "      parameters:\n"
        set column_names [lsort -unique $column_names]
      }

      foreach name $column_names {

        set type $data_types($name)

        set format ""
        set maxItems 1

        switch $type {
          integer {
            set type integer
            set format int32
          }
          bigint {
            set type integer
            set format int64
          }
          boolean - json_object {
            set type $type
          }
          numeric {
            set type number
            set format float
          }
          "double precision" {
            set type number
            set format double
          }
          number {
            set type number
          }
          date {
            set type "string"
            set format "date"
          }
          "timestamp with time zone" {
            set type "string"
            set format "date-time"
          }
          json_array {
            # We need to trick it into using items as well

            # We also need to check if we have a param doc for the array
            if {[info exists doc_elements(param_$name)]} {
              set type "array
        items:
          type: object
          properties:\n"
              foreach array_object_arg $doc_elements(param_$name) {
                append type "            [lindex $array_object_arg 0]:\n"
                set arg_type [lindex $array_object_arg 1]

                # Special handling of category type
                if {[lindex $array_object_arg 1] eq "category"} {
                  set category_type [lindex $array_object_arg 2]
                  append type "              type: integer\n"

                  if {[lrange $array_object_arg 3 end] ne ""} {
                    append type "              description: '[lrange $array_object_arg 3 end]'\n"
                  }
                  set allowed_category_ids [db_list categories "select category_id from im_categories where category_type = :category_type order by category_id"]
                  append type "              enum: \[[join $allowed_category_ids ","]\]"
                } else {
                  append type "              type: $arg_type\n"
                  append type "              description: '[lrange $array_object_arg 2 end]'\n"
                }
              }
            } else {
              set type "array
        items:
          type: string"
            }
            set maxItems ""
          }
          default {
            # No strict typing... assuming string..
            set type "string"            
            # Find out if we have an enum based on the dynfield widget associated with the acs_attribute
          }
        }
        
        # if it is required, minItems is 1
        set minItems $required_arr($name)

        
        # Create the reference to the component, only if it does not exist though
        set orig_name $name
        if {[info exists param_comp($name)]} {
          # It exists, store the old value for comparison
          set name "${operation}_${endpoint}_$name"
        }

        # initialize parameter component array
        set param_comp($name) "    $name:\n"

        append param_comp($name) "      name: $orig_name
      in: query\n"
        if {$column_description($orig_name) ne ""} {
          append param_comp($name) "      description: '$column_description($orig_name)'\n"
        }

        # Append the schema
        append param_comp($name) "      schema:
        type: $type\n"
        if {$format ne ""} {
          append param_comp($name) "        format: $format\n"
        }
        append param_comp($name) "        minItems: $minItems\n"
        if {$maxItems ne ""} {
          append param_comp($name) "        maxItems: 1\n"
        }

        # Append a default value if we have one
        if {[info exists default_values($name)] && $default_values($orig_name) ne ""} {
          set default_value [string trim [lindex [split $default_values($orig_name) "::"] 0] "'"]
          switch $default_value {
            'f' {
              set default_value "false"
            }
            't' {
              set default_value "true"
            }
          }
          append param_comp($name) "        default: [cog_rest::quotejson $default_value]\n"
        }

        if {[info exists param_comp($orig_name)]} {
          # we need to compare
          if {$param_comp($name) ne $param_comp($orig_name)} {
            ns_log Notice "$name differs... $param_comp($name) ...$param_comp($orig_name)"

            # need to append the param to paths
            append paths "      - \$ref: '#/components/parameters/$name'\n"
            lappend param_comps $name
          } else {
            append paths "      - \$ref: '#/components/parameters/$orig_name'\n"
            lappend param_comps $orig_name

            # No need to append the param_comps with the orig_name, as this should already exist
          }
        } else {
            append paths "      - \$ref: '#/components/parameters/$orig_name'\n"
            lappend param_comps $orig_name
        }
      }


      #---------------------------------------------------------------
      # Append the return values and examples
      #---------------------------------------------------------------

      if {$return_description eq ""} {
        set return_description "Generic description"
      }
      # Append the responses
      append paths "      responses:
        '200':
          description: $return_description
          content:
            application/json:
              schema:
                \$ref: '#/components/schemas/${operation}_$endpoint'\n"

    }
  }
}

#---------------------------------------------------------------
# Generate the components
#---------------------------------------------------------------
set components "components:\n \n"

# First loop through the parameters
append components "  parameters:\n"

foreach name $param_comps {
  # Not all column_names
  append components "$param_comp($name)\n"
}

set responses [lsort -unique $responses]
append components "  schemas:\n"

foreach response $responses {
  # Now for the responses
  append components "    $response:\n"
  append components "$response_comp($response)"
}

# Set the global tags
if {[llength $global_tags]>0} {
  set tags_yml "tags:\n"
  foreach tag $global_tags {
    append tags_yml "  -
    name: $tag\n"
  }
} else {
  set tags_yml ""
}

# Append security to components
append components "  securitySchemes:
    app_auth:
      type: http
      scheme: basic
      description: Basic Auth for the server"


#---------------------------------------------------------------
# API generic information
#---------------------------------------------------------------

# Get this form the package definiton
apm_version_get -package_key $package_key -array package
set api_version $package(version_name)
set api_description $package(description)
set pretty_name $package(pretty_plural)

set header "openapi: '3.0.2'
info:
  title: '$pretty_name Endpoints'
  description: '$api_description'
  version: '$api_version'
  contact:
    name: 'Malte Sussdorff'
    email: 'malte.sussdorff@cognovis.de'
  license:
    name: 'GPLv3'
    url: 'https://www.gnu.org/licenses/gpl-3.0.en.html'
$tags_yml
paths:
$paths
$components
servers:
  - url: '[im_system_url]/cognovis-rest'
    description: '[ad_system_name]'
security:
- app_auth: \[\]

"

ns_return 200 "text/plain" $header